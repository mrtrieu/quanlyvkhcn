﻿


using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
        IProductRepository Products { get; }
        IOrdersRepository Orders { get; }
        INewsRepository News { get;}
        ICongvandiRepository Congvandi { get; }
        ICongvandenRepository Congvanden { get; }
        IBaocaoRepository Baocao { get; }
        IFilemauRepository Filemau { get; }
        IFileDetailRepository FileDetails { get; }
        IProjectsRepository Projects { get; }
        IFileProjectRepository FileProjects { get; }
        int SaveChanges();
        int SaveChangesNoOverrideDate();
    }
}
