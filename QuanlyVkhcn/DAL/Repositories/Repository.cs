﻿
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;

        public DbSet<TEntity> Entity { get; }

        public Repository(DbContext context)
        {
            _context = context;
            Entity = context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            Entity.Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            Entity.AddRange(entities);
        }

        public virtual void Detach(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }
        public virtual void Update(TEntity entity)
        {
            Entity.Update(entity);
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities)
        {
            Entity.UpdateRange(entities);
        }



        public virtual void Remove(TEntity entity)
        {
            Entity.Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            Entity.RemoveRange(entities);
        }


        public virtual int Count()
        {
            return Entity.Count();
        }


        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Entity.Where(predicate);
        }

        public virtual TEntity GetSingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Entity.SingleOrDefault(predicate);
        }

        public virtual TEntity Get(int id)
        {
            return Entity.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Entity.ToList();
        }


        public virtual async Task<IEnumerable<TEntity>> GetPaginatedItem(Expression<Func<TEntity, bool>> predicate, int pageSize = 10, int pageIndex = 0)
        {
            return await Entity.
                Where(predicate)
                .Skip(pageSize * pageIndex)
                .Take(pageSize)
                .ToArrayAsync();
        }
    }
}
