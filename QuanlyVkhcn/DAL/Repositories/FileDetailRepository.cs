﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class FileDetailRepository : Repository<FileDetail>, IFileDetailRepository
    {
        public FileDetailRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public IEnumerable<FileDetail> GetFileDetailBytype(int typeId)
        {
            return _appContext.FileDetails.
                Where(o => o.TargetId == typeId).
                OrderBy(o=>o.CreatedDate);
        }
    }
}
