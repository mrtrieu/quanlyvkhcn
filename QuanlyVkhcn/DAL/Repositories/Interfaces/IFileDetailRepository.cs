﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace DAL.Repositories.Interfaces
{
    public interface IFileDetailRepository : IRepository<FileDetail>
    {

        IEnumerable<FileDetail> GetFileDetailBytype(int typeId);
    }
}
