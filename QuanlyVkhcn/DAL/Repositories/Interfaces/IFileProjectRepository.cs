﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace DAL.Repositories.Interfaces
{
    public interface IFileProjectRepository  :IRepository<FileProject>
    {

        IEnumerable<FileProject> GetFileProjectById(int id);
    }
}
