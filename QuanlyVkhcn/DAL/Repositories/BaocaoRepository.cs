﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class BaocaoRepository : Repository<Baocao>, IBaocaoRepository
    {
        public BaocaoRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public override  IEnumerable<Baocao> GetAll()
        {
            return Entity.Include(o => o.FileUpload)
                .Include(o => o.FilePublic)
                .ToList();
        }
    }
}
