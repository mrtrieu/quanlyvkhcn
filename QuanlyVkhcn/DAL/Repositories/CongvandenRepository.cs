﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories
{
    public class CongvandenRepository : Repository<Congvanden>, ICongvandenRepository
    {
        public CongvandenRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public override IEnumerable<Congvanden> GetAll()
        {
            return Entity.Include(o => o.FileUpload).ToList();
        }
    }
}
