﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAL.Repositories
{
    public class FileProjectRepository : Repository<FileProject>, IFileProjectRepository
    {
        public FileProjectRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;

        public override IEnumerable<FileProject> GetAll()
        {
            return Entity.Include(o => o.FileUpload).ToList();
        }

        public virtual IEnumerable<FileProject> GetFileProjectById(int id)
        {
            return Entity.Include(o => o.FileUpload)
                .Where(o => o.ProjectId == id).ToList();
        }

        public override FileProject GetSingleOrDefault(Expression<Func<FileProject, bool>> predicate)
        {
            return Entity.Include(o => o.AssginUser)
                .Include(o => o.ManagerUser)
                .SingleOrDefault(predicate);
        }
    }
}
