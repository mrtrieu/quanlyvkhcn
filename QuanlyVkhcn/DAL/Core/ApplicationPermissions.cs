﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DAL.Core
{
    public static class ApplicationPermissions
    {
        public static ReadOnlyCollection<ApplicationPermission> AllPermissions;


        public const string UsersPermissionGroupName = "User Permissions";
        public static ApplicationPermission ViewUsers = new ApplicationPermission("View Users", "users.view", UsersPermissionGroupName, "Permission to view other users account details");
        public static ApplicationPermission ManageUsers = new ApplicationPermission("Manage Users", "users.manage", UsersPermissionGroupName, "Permission to create, delete and modify other users account details");

        public const string RolesPermissionGroupName = "Role Permissions";
        public static ApplicationPermission ViewRoles = new ApplicationPermission("View Roles", "roles.view", RolesPermissionGroupName, "Permission to view available roles");
        public static ApplicationPermission ManageRoles = new ApplicationPermission("Manage Roles", "roles.manage", RolesPermissionGroupName, "Permission to create, delete and modify roles");
        public static ApplicationPermission AssignRoles = new ApplicationPermission("Assign Roles", "roles.assign", RolesPermissionGroupName, "Permission to assign roles to users");

        public const string FileProjectPermissionGroupName = "File Project Permissions";
        public static ApplicationPermission FpFileManage = new ApplicationPermission("Quản lý dự án", "fileProject.filemanage", FileProjectPermissionGroupName, "Quyền hạn được thêm xóa dự án");
        public static ApplicationPermission FpView = new ApplicationPermission("Xem file dự án", "fileProject.view", FileProjectPermissionGroupName, "Quyền hạn được xem dự án");
        public static ApplicationPermission FpEdit = new ApplicationPermission("Xóa file dự án", "fileProject.edit", FileProjectPermissionGroupName, "Quyền hạn được thay đổi dự án");
        public static ApplicationPermission FpApprove = new ApplicationPermission("Duyệt file dự án' ", "fileProject.approve", FileProjectPermissionGroupName, "Quyền hạn được duyệt dự án");
        public static ApplicationPermission FpAddFile = new ApplicationPermission("Cập nhật file dự án", "fileProject.addFile", FileProjectPermissionGroupName, "Quyền hạn được cập nhật dự án");

        public const string NewsPermissionGroupName = "News Permissions";
        public static ApplicationPermission NewsView = new ApplicationPermission("Xem tin tức", "news.view", NewsPermissionGroupName, "Quyền hạn được xem tin tức");
        public static ApplicationPermission NewsEdit = new ApplicationPermission("Chỉnh sửa tin tức", "news.edit", NewsPermissionGroupName, "Quyền hạn được cập nhật tin tức");


        public const string CongvanPermissionGroupName = "Document Permissions";
        public static ApplicationPermission CongVanView = new ApplicationPermission("Xem công văn", "congvan.view", CongvanPermissionGroupName, "Quyền hạn được xem tin tức");
        public static ApplicationPermission CongVanEdit = new ApplicationPermission("Chỉnh sửa công văn", "congvan.edit", CongvanPermissionGroupName, "Quyền hạn được cập nhật tin tức");

        static ApplicationPermissions()
        {
            List<ApplicationPermission> allPermissions = new List<ApplicationPermission>()
            {
                ViewUsers,
                ManageUsers,

                ViewRoles,
                ManageRoles,
                AssignRoles,

                FpFileManage,
                FpView,
                FpEdit,
                FpAddFile,
                FpApprove,

                NewsView,
                NewsEdit,

                CongVanView,
                CongVanEdit,
            };

            AllPermissions = allPermissions.AsReadOnly();
        }

        public static ApplicationPermission GetPermissionByName(string permissionName)
        {
            return AllPermissions.Where(p => p.Name == permissionName).FirstOrDefault();
        }

        public static ApplicationPermission GetPermissionByValue(string permissionValue)
        {
            return AllPermissions.Where(p => p.Value == permissionValue).FirstOrDefault();
        }

        public static string[] GetAllPermissionValues()
        {
            return AllPermissions.Select(p => p.Value).ToArray();
        }

        public static string[] GetAdministrativePermissionValues()
        {
            return new string[] { ManageUsers, ManageRoles, AssignRoles };
        }
    }



    public class ApplicationPermission
    {
        public ApplicationPermission()
        { }

        public ApplicationPermission(string name, string value, string groupName, string description = null)
        {
            Name = name;
            Value = value;
            GroupName = groupName;
            Description = description;
        }



        public string Name { get; set; }
        public string Value { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }


        public override string ToString()
        {
            return Value;
        }


        public static implicit operator string(ApplicationPermission permission)
        {
            return permission.Value;
        }
    }
}
