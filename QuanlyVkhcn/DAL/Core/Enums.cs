﻿
using System;

namespace DAL.Core
{
    public enum Gender
    {
        None,
        Female,
        Male
    }

    public enum FileProjectStatus
    {
        Init,
        Waiting,
        Decline,
        Approve,
        End,
    }
}
