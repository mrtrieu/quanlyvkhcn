﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class News : AuditableEntity
    {
        public int Id { get; set; }
        [MaxLength(500)]
        public string Title_VN { get; set; }
        [MaxLength(500)]
        public string Title_EN { get; set; }
        public string Content_EN { get; set; }
        public string Content_VN { get; set; }
        [MaxLength(255)]
        public string Image_EN { get; set; }
        [MaxLength(255)]
        public string Image_VN { get; set; }
        [MaxLength(500)]
        public string Category { get; set; }
        public bool IsView_VN { get; set; }
        public bool IsView_EN { get; set; }
    }
}
