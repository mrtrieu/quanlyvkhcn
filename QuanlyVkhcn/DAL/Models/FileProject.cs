﻿using DAL.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class FileProject : AuditableEntity
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Description { get; set; }
        [MaxLength(500)]
        public string Comment { get; set; }
        public int? FileUploadId { get; set; }
        public FileDetail FileUpload { get; set; }
        public string AssginUserId { get; set; }
        public string ManagerUserId { get; set; }
        public ApplicationUser AssginUser { get; set; }
        public ApplicationUser ManagerUser { get; set; }
        public FileProjectStatus Status { get; set; }
        public bool IsLimitUser { get; set; }
        public int? ProjectId { get; set; }
        public Project Project { get; set; }

    }
}
