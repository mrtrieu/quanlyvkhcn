﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Filemau: AuditableEntity
    {
        public int Id { get; set; }
        public string Loaifile { get; set; }
        public string Tenfile { get; set; }
        public DateTime Ngaytao { get; set; }
        public string Nguoitao { get; set; }
        public int? FileUploadId { get; set; }
        public FileDetail FileUpload { get; set; }
    }
}
