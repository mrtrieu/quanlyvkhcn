﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class FileDetail : AuditableEntity
    {
        public int Id { get; set; }
        [MaxLength(250)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Path { get; set; }
        [MaxLength(500)]
        public string Detail { get; set; }
        [MaxLength(20)]
        public string TargetType { get; set; }
        public int TargetId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
