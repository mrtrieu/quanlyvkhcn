﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Project: AuditableEntity
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Data { get; set; }

        public int? RelateProjectId { get; set; }
        public Product Parent { get; set; }
    }
}
