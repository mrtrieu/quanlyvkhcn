﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Congvandi: AuditableEntity
    {
        public int Id { get; set; }
        public string Loaicv { get; set; }
        public string Socv { get; set; }
        public string Ngaybh { get; set; }
        public string Noidungcv { get; set; }
        public string Nguoiky { get; set; }
        public string Noinhan { get; set; }
        public int? FileUploadId { get; set; }
        public FileDetail FileUpload { get; set; }
    }
}
