﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Baocao: AuditableEntity
    {
        public int Id { get; set; }
        public string Loaibc { get; set; }
        public string Tenbc { get; set; }
        public DateTime Ngaybc { get; set; }
        public string Nguoibc { get; set; }
        public int? FileUploadId { get; set; }
        public FileDetail FileUpload { get; set; }
        public int? FilePublicId { get; set; }
        public FileDetail FilePublic { get; set; }
    }
}
