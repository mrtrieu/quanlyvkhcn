﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        ICustomerRepository _customers;
        IProductRepository _products;
        IOrdersRepository _orders;
        INewsRepository _news;
        ICongvandiRepository _congvandi;
        ICongvandenRepository _congvanden;
        IBaocaoRepository _baocao;
        IFilemauRepository _filemau;
        IProjectsRepository _projects;
        IFileProjectRepository _fileProjects;
        IFileDetailRepository _fileDetails;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public ICustomerRepository Customers
        {
            get
            {
                if (_customers == null)
                    _customers = new CustomerRepository(_context);

                return _customers;
            }
        }

        public IProductRepository Products
        {
            get
            {
                if (_products == null)
                    _products = new ProductRepository(_context);
                return _products;
            }
        }



        public IOrdersRepository Orders
        {
            get
            {
                if (_orders == null)
                    _orders = new OrdersRepository(_context);
                return _orders;
            }
        }


        public INewsRepository News

        {
            get
            {
                if (_news == null)
                    _news = new NewsRepository(_context);
                return _news;
            }
        }
        public ICongvandiRepository Congvandi

        {
            get
            {
                if (_congvandi == null)
                    _congvandi = new CongvandiRepository(_context);
                return _congvandi;
            }
        }
        public ICongvandenRepository Congvanden

        {
            get
            {
                if (_congvanden == null)
                    _congvanden = new CongvandenRepository(_context);
                return _congvanden;
            }
        }
        public IBaocaoRepository Baocao

        {
            get
            {
                if (_baocao == null)
                    _baocao = new BaocaoRepository(_context);
                return _baocao;
            }
        }
        public IFilemauRepository Filemau

        {
            get
            {
                if (_filemau == null)
                    _filemau = new FilemauRepository(_context);
                return _filemau;
            }
        }
        public IProjectsRepository Projects
        {
            get
            {
                if (_projects == null)
                    _projects = new ProjectRepository(_context);
                return _projects;
            }
        }

        public IFileDetailRepository FileDetails
        {
            get
            {
                if (_fileDetails == null)
                    _fileDetails = new FileDetailRepository(_context);
                return _fileDetails;
            }
        }

        public IFileProjectRepository FileProjects
        {
            get
            {
                if (_fileProjects == null)
                    _fileProjects = new FileProjectRepository(_context);
                return _fileProjects;
            }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public int SaveChangesNoOverrideDate()
        {
            return _context.SaveChangesNoOverrideDate();
        }
    }
}
