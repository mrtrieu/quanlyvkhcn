﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CongvandenController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public CongvandenController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Congvanden
        [HttpGet]
        public IActionResult Get()
        {

            var allCongvanden = this._unitOfWork.Congvanden.GetAll();
            return Ok(Mapper.Map<IEnumerable<CongvandenViewModel>>(allCongvanden));
        }

        // GET: api/Congvanden/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var congvanden = _unitOfWork.Congvanden.GetSingleOrDefault(o => o.Id == id);
            if (congvanden != null)
            {
                return Ok(Mapper.Map<CongvandenViewModel>(congvanden));
            }
            return NotFound();
        }
        
        

        // POST: api/Congvanden
        [HttpPost]
        public IActionResult Post([FromBody] CongvandenViewModel congvanden)
        {
            var congvandenData = Mapper.Map<Congvanden>(congvanden);
            _unitOfWork.Congvanden.Add(congvandenData);
            _unitOfWork.SaveChanges();
            return Ok(congvandenData);
        }

        // PUT: api/Congvanden/5
        [HttpPut("{id}")] 
        public IActionResult Put(int id, [FromBody]  CongvandenViewModel congvanden)
        {
            congvanden.Id = id;
            var checkItem = _unitOfWork.Congvanden.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                //kiem tra fieupload co thay doi ko
                if (congvanden.FileUploadId != checkItem.FileUploadId)
                {
                    var oldFile = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == checkItem.FileUploadId);
                    oldFile.TargetType = "delete";
                    oldFile.TargetId = 0;
                    _unitOfWork.SaveChanges();
                }
                _unitOfWork.Congvanden.Detach(checkItem);
                var congvandenData = Mapper.Map<Congvanden>(congvanden);
                congvandenData.Id = id;
                _unitOfWork.Congvanden.Update(congvandenData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.Congvanden.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Congvanden.Remove(checkItem);
                _unitOfWork.SaveChanges();
            }
            return NotFound("Not found");
        }
    }
}
