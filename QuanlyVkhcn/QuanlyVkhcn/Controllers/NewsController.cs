﻿using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using QuanlyVkhcn.ViewModels;
using System.Collections.Generic;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public NewsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/News
        [HttpGet]
        public IActionResult Get()
        {

            var allNews = this._unitOfWork.News.GetAll();
            return Ok(Mapper.Map<IEnumerable<NewsViewModel>>(allNews));
        }
        
        // GET: api/News/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var news = _unitOfWork.News.GetSingleOrDefault(o => o.Id == id);
            if (news != null)
            {
                return Ok(Mapper.Map<NewsViewModel>(news));
            }
            return NotFound();
        }
        

        // POST: api/News
        [HttpPost]
        public void Post([FromBody] NewsViewModel news)
        {
            var newsData = Mapper.Map<News>(news);
            _unitOfWork.News.Add(newsData);
            _unitOfWork.SaveChanges();
        }
       
        // PUT: api/News/5
        [HttpPut("{id}")] 
        public IActionResult Put(int id, [FromBody]  NewsViewModel news)
        {
            news.Id = id;
            var checkItem = _unitOfWork.News.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.News.Detach(checkItem);
                var newsData = Mapper.Map<News>(news);
                newsData.Id = id;
                _unitOfWork.News.Update(newsData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.News.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.News.Remove(checkItem);
                _unitOfWork.SaveChanges();
            }
            return NotFound("Not found");
        }
    }
}
