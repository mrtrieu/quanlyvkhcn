﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using QuanlyVkhcn.Helpers;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    public class HomeController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailSender _emailer;
        private readonly IHtmlLocalizer<MultiLanguage> _localizer;
        public HomeController(IUnitOfWork unitOfWork, IEmailSender emailSender, IHtmlLocalizer<MultiLanguage> localizer)
        {
            _unitOfWork = unitOfWork;
            _emailer = emailSender;
            _localizer = localizer;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("about-us")]
        public IActionResult About()
        {

            return View();
        }

        [Route("contact")]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            TempData["title"] = _localizer["Thank"].Value;
            TempData["message"] = _localizer["ContactEmailSended"].Value;
            return RedirectToAction("Message");
        }

        public IActionResult Project()
        {
            return View();
        }


        public IActionResult Message()
        {
            ViewBag.Title = TempData["title"];
            ViewBag.Message = TempData["message"];
            return View();
        }
        [Route("news")]
        public async Task<IActionResult> News([FromQuery]int page = 1)
        {
            var itemsPerPage = 10;
            if (page < 1) page = 1;
            var allNews = await _unitOfWork.News.GetPaginatedItem(o => o.IsView_VN, itemsPerPage, page - 1);
            var total = _unitOfWork.News.Count();
            ViewBag.paginate = new PageHeader(page, itemsPerPage, total);
            return View(Mapper.Map<IEnumerable<NewsViewModel>>(allNews));
        }

        [Route("news/{id}")]
        public IActionResult NewsDetail(int id)
        {
            var news = _unitOfWork.News.GetSingleOrDefault(o => o.Id == id);
            if (news != null)
            {
                return View(Mapper.Map<NewsViewModel>(news));
            }
            return NotFound();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("error/{code:int}")]
        public IActionResult Error(int code)
        {
            ViewBag.code = code;
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Email([FromForm]  string email, [FromForm] string name, [FromForm] string subject, [FromForm] string message)
        {
            var dpistEmail = "longtrieu1912@gmail.com";
            //var dpistEmail = "dpist@dut.edu.vn";
            if (!string.IsNullOrEmpty(email))
            {
                var content = $"Nhận được thư thông qua trang web của {name} từ địa chỉ {email}.<br/> với nội dung {message} ";
                await _emailer.SendEmailAsync(
                    name,
                    dpistEmail,
                    $"[Website] {subject}",
                    email
                    );
            }
            TempData["title"] = _localizer["Thank"].Value;
            TempData["message"] = _localizer["ContactEmailSended"].Value;
            return RedirectToAction("Message");
        }

        public IActionResult SetCulture(string id = "en")
        {
            string culture = id;
            Response.Cookies.Append(
               CookieRequestCultureProvider.DefaultCookieName,
               CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
               new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
           );

            ViewData["Message"] = "Culture set to " + culture;
            return View("About");
        }



        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}
