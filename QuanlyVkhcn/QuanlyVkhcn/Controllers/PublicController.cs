﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Mvc;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    public class PublicController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        public PublicController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }




        public IActionResult Index()
        {
            var allBaocao = this._unitOfWork.Baocao.GetAll();
            return View(AutoMapper.Mapper.Map<IEnumerable<BaocaoViewModel>>(allBaocao));
        }

        
        [HttpGet("{id}")]
        public IActionResult Share(int id)
        {
            var data = this._unitOfWork.Baocao.GetSingleOrDefault(o => o.Id == id);
            if (data!=null)
            {
                var mapData= AutoMapper.Mapper.Map<BaocaoViewModel>(data);
                return View(mapData);
            }
            return NotFound();
        }
    

    }
}
