﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaocaoController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public BaocaoController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Baocao
        [HttpGet]
        public IActionResult Get()
        {

            var allBaocao = this._unitOfWork.Baocao.GetAll();
            return Ok(Mapper.Map<IEnumerable<BaocaoViewModel>>(allBaocao));
        }
        
        // GET: api/Baocao/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var baocao = _unitOfWork.Baocao.GetSingleOrDefault(o => o.Id == id);
            if (baocao != null)
            {
                return Ok(Mapper.Map<BaocaoViewModel>(baocao));
            }
            return NotFound();
        }
        

        // POST: api/Baocao
        [HttpPost]
        public IActionResult Post([FromBody] BaocaoViewModel baocao)
        {
            var baocaoData = Mapper.Map<Baocao>(baocao);
            _unitOfWork.Baocao.Add(baocaoData);
            _unitOfWork.SaveChanges();
            return Ok(baocaoData);
        }

        // PUT: api/Baocao/5
        [HttpPut("{id}")] 
        public IActionResult Put(int id, [FromBody]  BaocaoViewModel baocao)
        {
            baocao.Id = id;
            var checkItem = _unitOfWork.Baocao.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                //kiem tra fieupload co thay doi ko
                if (baocao.FileUploadId != checkItem.FileUploadId)
                {
                    var oldFile = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == checkItem.FileUploadId);
                    oldFile.TargetType = "delete";
                    oldFile.TargetId = 0;
                    _unitOfWork.SaveChanges();
                }
                _unitOfWork.Baocao.Detach(checkItem);
                var baocaoData = Mapper.Map<Baocao>(baocao);
                baocaoData.Id = id;
                _unitOfWork.Baocao.Update(baocaoData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.Baocao.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Baocao.Remove(checkItem);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound("Not found");
        }
    }
}
