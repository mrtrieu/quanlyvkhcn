﻿using AutoMapper;
using DAL;
using DAL.Core.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using QuanlyVkhcn.Helpers;
using QuanlyVkhcn.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
namespace QuanlyVkhcn.Controllers
{

    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FileProjectController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private IHostingEnvironment _hostingEnvironment;
        readonly IEmailSender _emailer;
        public FileProjectController(IUnitOfWork unitOfWork,
            IHostingEnvironment hostingEnvironment,
            IAccountManager accountManager
            , IEmailSender emailer)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _emailer = emailer;
        }

        // GET: api/FileProject
        [HttpGet]
        public IActionResult Get()
        {

            var allData = this._unitOfWork.FileProjects.GetAll();
            return Ok(Mapper.Map<IEnumerable<FileProjectViewModel>>(allData));
        }

        [HttpGet("project/{id}")]
        public IActionResult GetByProject(int id)
        {

            var allData = this._unitOfWork.FileProjects.GetFileProjectById(id);
            return Ok(Mapper.Map<IEnumerable<FileProjectViewModel>>(allData));
        }

        // GET: api/FileProjects/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var data = _unitOfWork.FileProjects.GetSingleOrDefault(o => o.Id == id);
            if (data != null)
            {
                return Ok(Mapper.Map<FileProjectViewModel>(data));
            }
            return NotFound();
        }



        // POST: api/FileProjects
        [HttpPost]
        public IActionResult Post([FromBody] FileProjectViewModel news)
        {
            try
            {
                var postData = Mapper.Map<FileProject>(news);
                _unitOfWork.FileProjects.Add(postData);
                _unitOfWork.SaveChangesNoOverrideDate();
                return Created("", postData);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }



        // PUT: api/FileProjects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]  FileProjectViewModel putData)
        {
            putData.Id = id;
            var checkItem = _unitOfWork.FileProjects.
                GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                if (putData.FileUploadId != checkItem.FileUploadId && checkItem.FileUploadId > 0)
                {
                    var oldFile = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == checkItem.FileUploadId);
                    if (oldFile != null)
                    {
                        oldFile.TargetType = "delete-fileproject";
                        _unitOfWork.SaveChanges();
                    }
                }
                _unitOfWork.FileProjects.Detach(checkItem);
                var data = Mapper.Map<FileProject>(putData);
                data.Id = id;
                _unitOfWork.FileProjects.Update(data);
                _unitOfWork.SaveChangesNoOverrideDate();
                await ProcessFileProjectSendEmail(data);
                return Ok();
            }
            return NotFound();
        }

        public async Task ProcessFileProjectSendEmail(FileProject item)
        {

            var fp = _unitOfWork.FileProjects.
               GetSingleOrDefault(o => o.Id == item.Id);
            if (fp.Status == DAL.Core.FileProjectStatus.Waiting)
            {
                var dic = new Dictionary<string, string>();
                if (fp.ManagerUser != null)
                {
                    dic.Add("user", fp.ManagerUser.UserName);
                    dic.Add("name", fp.Name);
                    dic.Add("description", fp.Comment);
                    var email = EmailTemplates.GetEmailTemplate(EmailTemplates.FP_UPDATE, dic);
                    await _emailer.SendEmailAsync(
                        fp.ManagerUser.UserName,
                        fp.ManagerUser.Email,
                        $"File {fp.Name} đã thay đổi",
                        email
                        );
                }
            }

            if (fp.Status == DAL.Core.FileProjectStatus.Approve)
            {
                var dic = new Dictionary<string, string>();
                if (fp.AssginUser != null)
                {
                    dic.Add("user", fp.AssginUser.UserName);
                    dic.Add("name", fp.Name);
                    dic.Add("description", fp.Comment);
                    var email = EmailTemplates.GetEmailTemplate(EmailTemplates.FP_ACCEPT, dic);
                    await _emailer.SendEmailAsync(
                        fp.ManagerUser.UserName,
                        fp.ManagerUser.Email,
                        $"File {fp.Name} đã được duyệt",
                        email
                        );
                }
            }

            if (fp.Status == DAL.Core.FileProjectStatus.Decline)
            {
                var dic = new Dictionary<string, string>();
                if (fp.ManagerUser != null)
                {
                    dic.Add("user", fp.ManagerUser.UserName);
                    dic.Add("name", fp.Name);
                    dic.Add("description", fp.Comment);
                    var email = EmailTemplates.GetEmailTemplate(EmailTemplates.FP_DECLINE, dic);
                    await _emailer.SendEmailAsync(
                        fp.ManagerUser.UserName,
                        fp.ManagerUser.Email,
                        $"File {fp.Name} cần được chỉnh sửa",
                        email
                        );
                }
            }
        }



        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var checkItem = _unitOfWork.FileProjects.GetSingleOrDefault(o => o.Id == id);
                if (checkItem != null)
                {
                    _unitOfWork.FileProjects.Remove(checkItem);
                    _unitOfWork.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception)
            {
                return NoContent();
            }
            return NotFound();
        }

        [HttpGet("file/type/{id}")]
        public IActionResult FileGetByType(int id)
        {
            if (id == 0) return NotFound();
            var data = _unitOfWork.FileDetails.GetFileDetailBytype(id);
            if (data != null)
            {
                return Ok(Mapper.Map<IEnumerable<FileDetailViewModel>>(data));
            }
            return NotFound();
        }

        // GET: api/FileProjects/5
        [HttpGet("file/{id}")]
        public IActionResult FileGet(int id)
        {
            var data = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == id);
            if (data != null)
            {
                return Ok(Mapper.Map<FileDetailViewModel>(data));
            }
            return NotFound();
        }

        // POST: api/FileProjects
        [HttpPost("file/{id}")]
        public void FilePost([FromBody] FileDetailViewModel news)
        {
            var postData = Mapper.Map<FileDetail>(news);
            _unitOfWork.FileDetails.Add(postData);
            _unitOfWork.SaveChanges();
        }


        // PUT: api/FileProjects/5
        [HttpPut("file/{id}")]
        public IActionResult FilePut(int id, [FromBody]  FileDetailViewModel putData)
        {
            putData.Id = id;
            var checkItem = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.FileDetails.Detach(checkItem);
                var data = Mapper.Map<FileDetail>(putData);
                data.Id = id;
                _unitOfWork.FileDetails.Update(data);
                _unitOfWork.SaveChanges();

                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("file/{id}")]
        public IActionResult FileDelete(int id)
        {
            var checkItem = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.FileDetails.Remove(checkItem);
                _unitOfWork.SaveChanges();
            }
            return NotFound();
        }

        [HttpPost("upload"), DisableRequestSizeLimit]
        public ActionResult UploadFile([FromQuery] string targetType)
        {
            try
            {
                var file = Request.Form.Files[0];
                string targetTypex = string.IsNullOrEmpty(targetType) ? Request.Form["targetType"].ToString() : targetType;
                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                string fileName = file.FileName;
                if (!Utilities.ValidateUploadFileType(file.ContentType) || GetContentType(file.FileName) == null)
                {
                    return Unauthorized();
                }
                var fileConfig = Utilities.GetFileDetailSetting(fileName, targetTypex);
                newPath = Path.Combine(newPath, fileConfig.Folder);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                string fullPath = Path.Combine(newPath, fileConfig.FileName);
                var targetId = 0;
                int.TryParse(Request.Form["targetId"], out targetId);
                var fileDetail = new FileDetail()
                {
                    Name = fileName,
                    Path = fullPath,
                    TargetType = targetTypex,
                    TargetId = targetId,
                    Detail = Request.Form["detail"]

                };
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }

                _unitOfWork.FileDetails.Add(fileDetail);
                _unitOfWork.SaveChanges();

                var imageUrl = fileDetail.Path.Substring(fileDetail.Path.IndexOf("wwwroot") + 7,
                    (fileDetail.Path.Length - fileDetail.Path.IndexOf("wwwroot") - 7));
                imageUrl = imageUrl.Replace("\\", "/");
                return Json(new { message = "Done", data = fileDetail, ok = true, imageUrl });
            }
            catch (System.Exception ex)
            {
                return Json(new { message = "Upload Failed: " + ex.Message, ok = false });
            }
        }

        [HttpGet("download/{id}"), DisableRequestSizeLimit]
        public async Task<IActionResult> Download(int id, string req)
        {
            var file = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == id);
            if (file != null)
            {

                if (System.IO.File.Exists(file.Path))
                {
                    try
                    {
                        var memory = new MemoryStream();
                        using (var stream = new FileStream(file.Path, FileMode.Open))
                        {
                            await stream.CopyToAsync(memory);
                        }
                        memory.Position = 0;
                        Response.Headers.Add("X-Frame-Options", "DENY");
                        return File(memory, GetContentType(file.Path), file.Name);
                    }
                    catch
                    {
                        return NotFound(new { message = "Download Failed: ", ok = false });
                    }

                }

            }
            return NotFound(new { message = "Download Failed: ", ok = false });
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }


    }
}
