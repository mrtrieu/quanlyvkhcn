﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CongvandiController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public CongvandiController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Congvandi
        [HttpGet]
        public IActionResult Get()
        {

            var allCongvandi = this._unitOfWork.Congvandi.GetAll();
            return Ok(Mapper.Map<IEnumerable<CongvandiViewModel>>(allCongvandi));
        }

        // GET: api/Congvandi/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var congvandi = _unitOfWork.Congvandi.GetSingleOrDefault(o => o.Id == id);
            if (congvandi != null)
            {
                return Ok(Mapper.Map<CongvandiViewModel>(congvandi));
            }
            return NotFound();
        }


        // POST: api/Congvandi
        [HttpPost]
        public IActionResult Post([FromBody] CongvandiViewModel congvandi)
        {
            var congvandiData = Mapper.Map<Congvandi>(congvandi);
            _unitOfWork.Congvandi.Add(congvandiData);
            _unitOfWork.SaveChanges();
            return Ok(congvandiData);
        }

        // PUT: api/Congvandi/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]  CongvandiViewModel congvandi)
        {
            congvandi.Id = id;
            var checkItem = _unitOfWork.Congvandi.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                //kiem tra fieupload co thay doi ko
                if (congvandi.FileUploadId != checkItem.FileUploadId)
                {
                    var oldFile = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == checkItem.FileUploadId);
                    oldFile.TargetType = "delete";
                    oldFile.TargetId = 0;
                    _unitOfWork.SaveChanges();
                }
                _unitOfWork.Congvandi.Detach(checkItem);
                var congvandiData = Mapper.Map<Congvandi>(congvandi);
                congvandiData.Id = id;
                _unitOfWork.Congvandi.Update(congvandiData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.Congvandi.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Congvandi.Remove(checkItem);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound("Not found");
        }
    }
}
