﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilemauController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public FilemauController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Filemau
        [HttpGet]
        public IActionResult Get()
        {

            var allFilemau = this._unitOfWork.Filemau.GetAll();
            return Ok(Mapper.Map<IEnumerable<FilemauViewModel>>(allFilemau));
        }

        // GET: api/Filemau/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var filemau = _unitOfWork.Filemau.GetSingleOrDefault(o => o.Id == id);
            if (filemau != null)
            {
                return Ok(Mapper.Map<FilemauViewModel>(filemau));
            }
            return NotFound();
        }


        // POST: api/Filemau
        [HttpPost]
        public IActionResult Post([FromBody] FilemauViewModel filemau)
        {
            var filemauData = Mapper.Map<Filemau>(filemau);
            _unitOfWork.Filemau.Add(filemauData);
            _unitOfWork.SaveChanges();
            return Ok(filemauData);
        }

        // PUT: api/Filemau/5
        [HttpPut("{id}")] 
        public IActionResult Put(int id, [FromBody]  FilemauViewModel filemau)
        {
            filemau.Id = id;
            var checkItem = _unitOfWork.Filemau.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                //kiem tra fieupload co thay doi ko
                if (filemau.FileUploadId != checkItem.FileUploadId)
                {
                    var oldFile = _unitOfWork.FileDetails.GetSingleOrDefault(o => o.Id == checkItem.FileUploadId);
                    oldFile.TargetType = "delete";
                    oldFile.TargetId = 0;
                    _unitOfWork.SaveChanges();
                }
                _unitOfWork.Filemau.Detach(checkItem);
                var filemauData = Mapper.Map<Filemau>(filemau);
                filemauData.Id = id;
                _unitOfWork.Filemau.Update(filemauData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.Filemau.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Filemau.Remove(checkItem);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound("Not found");
        }
    }
}
