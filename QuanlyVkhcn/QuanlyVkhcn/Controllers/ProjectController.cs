﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuanlyVkhcn.ViewModels;

namespace QuanlyVkhcn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {

        private readonly IUnitOfWork _unitOfWork;
        public ProjectController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Baocao
        [HttpGet]
        public IActionResult Get()
        {

            var data = this._unitOfWork.Projects.GetAll();
            return Ok(Mapper.Map<IEnumerable<ProjectViewModel>>(data));
        }
        
        // GET: api/Baocao/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var baocao = _unitOfWork.Projects.GetSingleOrDefault(o => o.Id == id);
            if (baocao != null)
            {
                return Ok(Mapper.Map<ProjectViewModel>(baocao));
            }
            return NotFound();
        }
        

        // POST: api/Baocao
        [HttpPost]
        public IActionResult Post([FromBody] ProjectViewModel postData)
        {
            var data = Mapper.Map<Project>(postData);
            _unitOfWork.Projects.Add(data);
            _unitOfWork.SaveChanges();
            return Ok(postData);
        }

        // PUT: api/Baocao/5
        [HttpPut("{id}")] 
        public IActionResult Put(int id, [FromBody]  ProjectViewModel baocao)
        {
            baocao.Id = id;
            var checkItem = _unitOfWork.Projects.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Projects.Detach(checkItem);
                var baocaoData = Mapper.Map<Project>(baocao);
                baocaoData.Id = id;
                _unitOfWork.Projects.Update(baocaoData);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var checkItem = _unitOfWork.Projects.GetSingleOrDefault(o => o.Id == id);
            if (checkItem != null)
            {
                _unitOfWork.Projects.Remove(checkItem);
                _unitOfWork.SaveChanges();
                return Ok();
            }
            return NotFound("Not found");
        }
    }
}
