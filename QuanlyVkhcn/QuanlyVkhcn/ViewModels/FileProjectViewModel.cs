﻿using DAL.Core;
using DAL.Models;
using System;

namespace QuanlyVkhcn.ViewModels
{
    public class FileProjectViewModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public FileDetailViewModel FileUpload { get; set; }
        public int? FileUploadId { get; set; }
        public string OldFileUploadId { get; set; }
        public string AssginUserID { get; set; }
        public string ManagerUserId { get; set; }
        public FileProjectStatus Status { get; set; }
        public bool IsLimitUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
