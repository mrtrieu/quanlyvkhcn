﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanlyVkhcn.ViewModels
{
    public class CongvandenViewModel
    {
        public int Id { get; set; }
        public string Loaicv { get; set; }
        public string Socv { get; set; }
        public string Ngaybh { get; set; }
        public string Noidungcv { get; set; }
        public string Cqbh { get; set; }
        public string Ngaynhan{ get; set; }
        public int? FileUploadId { get; set; }
        public FileDetailViewModel FileUpload { get; set; }
    }
}
