﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanlyVkhcn.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public string Title_VN { get; set; }
        public string Title_EN { get; set; }
        public string Content_EN { get; set; }
        public string Content_VN { get; set; }
        public string Image_EN { get; set; }
        public string Image_VN { get; set; }
        public bool IsView_VN { get; set; }
        public bool IsView_EN { get; set; }
    }
}
