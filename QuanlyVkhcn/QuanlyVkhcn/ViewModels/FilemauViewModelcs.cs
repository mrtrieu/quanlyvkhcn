﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanlyVkhcn.ViewModels
{
    public class FilemauViewModel
    {
        public int Id { get; set; }
        public string Loaifile { get; set; }
        public string Tenfile { get; set; }
        public DateTime Ngaytao { get; set; }
        public string Nguoitao{ get; set; }
        public int? FileUploadId { get; set; }
        public FileDetailViewModel FileUpload { get; set; }
    }
}
