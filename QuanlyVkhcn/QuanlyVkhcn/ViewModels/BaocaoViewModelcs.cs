﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanlyVkhcn.ViewModels
{
    public class BaocaoViewModel
    {
        public int Id { get; set; }
        public string Loaibc { get; set; }
        public string Tenbc { get; set; }
        public DateTime Ngaybc { get; set; }
        public string Nguoibc{ get; set; }
        public int? FileUploadId { get; set; }
        public FileDetailViewModel FileUpload { get; set; }
        public int? FilePublicId { get; set; }
        public FileDetailViewModel FilePublic { get; set; }
    }
}
