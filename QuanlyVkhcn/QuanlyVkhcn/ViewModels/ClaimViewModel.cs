﻿
using System;
using System.Linq;

namespace QuanlyVkhcn.ViewModels
{
    public class ClaimViewModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
