﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanlyVkhcn.ViewModels
{
    public class FileDetailViewModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Detail { get; set; }
        public string TargetType { get; set; }
        public int TargetId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public UserPatchViewModel User { get; set; }
    }
}
