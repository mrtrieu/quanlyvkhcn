﻿
using AspNet.Security.OpenIdConnect.Primitives;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QuanlyVkhcn.Helpers
{
    public static class Utilities
    {
        static ILoggerFactory _loggerFactory;
        static Random randomGenerator = new Random();

        public static void ConfigureLogger(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }


        public static ILogger CreateLogger<T>()
        {
            if (_loggerFactory == null)
            {
                throw new InvalidOperationException($"{nameof(ILogger)} is not configured. {nameof(ConfigureLogger)} must be called before use");
                //_loggerFactory = new LoggerFactory().AddConsole().AddDebug();
            }

            return _loggerFactory.CreateLogger<T>();
        }


        public static void QuickLog(string text, string filename)
        {
            string dirPath = Path.GetDirectoryName(filename);

            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            using (StreamWriter writer = File.AppendText(filename))
            {
                writer.WriteLine($"{DateTime.Now} - {text}");
            }
        }



        public static string GetUserId(ClaimsPrincipal user)
        {
            return user.FindFirst(OpenIdConnectConstants.Claims.Subject)?.Value?.Trim();
        }



        public static string[] GetRoles(ClaimsPrincipal identity)
        {
            return identity.Claims
                .Where(c => c.Type == OpenIdConnectConstants.Claims.Role)
                .Select(c => c.Value)
                .ToArray();
        }

        public static bool ValidateUploadFileType(string contentType)
        {
            var allowType = new[]{"application/pdf",
              "image/png",
              "image/jpeg",
              "application/msword",
              "application/octet-stream",
              "application/vnd.ms-excel",
              "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            };

            return allowType.FirstOrDefault(o => o == contentType) != null;
        }

        public static FileDetailSetting GetFileDetailSetting(string fileName, string fileType)
        {
            var result = new FileDetailSetting();
            var today = DateTime.Now;
            switch (fileType)
            {
                case "fileproject":
                    result.Folder = "file-project";
                    result.FileName = $"{today.Year}{today.Month}{today.Day}-{GenerateRandomString(5)}-{fileName}";
                    break;
                case "congvanden":
                case "congvandi":
                    result.Folder = "congvan";
                    result.FileName = $"{today.Year}{today.Month}{today.Day}-{GenerateRandomString(5)}-{fileName}";
                    break;
                case "baocao":
                    result.Folder = "baocao";
                    result.FileName = $"{today.Year}{today.Month}{today.Day}-{GenerateRandomString(5)}-{fileName}";
                    break;
                case "filetemplate":
                    result.Folder = "template";
                    result.FileName = $"{today.Year}{today.Month}{today.Day}-{GenerateRandomString(5)}-{fileName}";
                    break;
                case "image":
                    result.Folder = "image";
                    result.FileName = $"{today.Year}{today.Month}{today.Day}-{GenerateRandomString(5)}-{fileName}";
                    break;
            }
            return result;
        }
        internal static string GenerateRandomString(int length)
        {
            const string pool = "abcdefghijklmnopqrstuvwxyz0123456789";
            var builder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                var c = pool[randomGenerator.Next(0, pool.Length)];
                builder.Append(c);
            }
            return builder.ToString();
        }

        internal static string MD5Ecnrypt(string text)
        {
            using (var md5 = MD5.Create())
            {
                return string.Join("", md5.ComputeHash(Encoding.ASCII.GetBytes(text)).Select(x => x.ToString("X2")));
            }
        }

        public static string HTMLtoText(string htmlString)
        {
            return Regex.Replace(htmlString, @"<(.|\n)*?>", "");
        }

        public static string TrimHtmlText(string htmlString, int length)
        {
            var html = HTMLtoText(htmlString);
            if (html.Length > length)
            {
                html = html.Substring(0, length);
                html = html.Substring(0, html.LastIndexOf(" "));
                html += "...";
            }
            return html;
        }
    }
}


public class FileDetailSetting
{

    public string FileName { get; set; }
    public string Folder { get; set; }
}