﻿
using System;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Collections.Generic;

namespace QuanlyVkhcn.Helpers
{
    public static class EmailTemplates
    {
        static IHostingEnvironment _hostingEnvironment;
        static string testEmailTemplate;
        static string plainTextTestEmailTemplate;


        public const string FP_ACCEPT = "fp-accept";

        public const string FP_DECLINE = "fp-decline";

        public const string FP_UPDATE = "fp-update";

        public static void Initialize(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }


        public static string GetEmailTemplate(string type, Dictionary<string, string> data)
        {
            var path = "Helpers/Templates/TestEmail.template";
            switch (type)
            {
                case FP_ACCEPT:
                    path = "Helpers/Templates/fileproject-accept.template";
                    break;
                case FP_DECLINE:
                    path = "Helpers/Templates/fileproject-decline.template";
                    break;
                case FP_UPDATE:
                    path = "Helpers/Templates/fileproject-update.template";
                    break;

            }

            var emailTemplate = ReadPhysicalFile(path);

            foreach (var item in data)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    emailTemplate = emailTemplate.Replace($"{{{item.Key}}}", item.Value);
                }
                else
                {
                    emailTemplate = emailTemplate.Replace($"{{{item.Key}}}", "");
                }
            }
            emailTemplate = emailTemplate.Replace("{now}", DateTime.Now.ToShortDateString());
            return emailTemplate;
        }

        public static string GetTestEmail(string recepientName, DateTime testDate)
        {
            if (testEmailTemplate == null)
                testEmailTemplate = ReadPhysicalFile("Helpers/Templates/TestEmail.template");
            string emailMessage = testEmailTemplate
                .Replace("{user}", recepientName)
                .Replace("{testDate}", testDate.ToString());
            return emailMessage;
        }



        public static string GetPlainTextTestEmail(DateTime date)
        {
            if (plainTextTestEmailTemplate == null)
                plainTextTestEmailTemplate = ReadPhysicalFile("Helpers/Templates/PlainTextTestEmail.template");


            string emailMessage = plainTextTestEmailTemplate
                .Replace("{date}", date.ToString());

            return emailMessage;
        }




        private static string ReadPhysicalFile(string path)
        {
            if (_hostingEnvironment == null)
                throw new InvalidOperationException($"{nameof(EmailTemplates)} is not initialized");

            IFileInfo fileInfo = _hostingEnvironment.ContentRootFileProvider.GetFileInfo(path);

            if (!fileInfo.Exists)
                throw new FileNotFoundException($"Template file located at \"{path}\" was not found");

            using (var fs = fileInfo.CreateReadStream())
            {
                using (var sr = new StreamReader(fs))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
}
