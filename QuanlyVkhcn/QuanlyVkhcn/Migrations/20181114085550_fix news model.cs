﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class fixnewsmodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsView_VN",
                table: "AppNews",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsView_EN",
                table: "AppNews",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IsView_VN",
                table: "AppNews",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<string>(
                name: "IsView_EN",
                table: "AppNews",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
