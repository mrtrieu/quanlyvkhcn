﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class fixfpuseridstring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_AssginUserId1",
                table: "AppFileProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_ManagerUserId1",
                table: "AppFileProjects");

            migrationBuilder.DropIndex(
                name: "IX_AppFileProjects_AssginUserId1",
                table: "AppFileProjects");

            migrationBuilder.DropIndex(
                name: "IX_AppFileProjects_ManagerUserId1",
                table: "AppFileProjects");

            migrationBuilder.DropColumn(
                name: "AssginUserId1",
                table: "AppFileProjects");

            migrationBuilder.DropColumn(
                name: "ManagerUserId1",
                table: "AppFileProjects");

            migrationBuilder.AlterColumn<string>(
                name: "ManagerUserId",
                table: "AppFileProjects",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AssginUserId",
                table: "AppFileProjects",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_AssginUserId",
                table: "AppFileProjects",
                column: "AssginUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_ManagerUserId",
                table: "AppFileProjects",
                column: "ManagerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_AssginUserId",
                table: "AppFileProjects",
                column: "AssginUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_ManagerUserId",
                table: "AppFileProjects",
                column: "ManagerUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_AssginUserId",
                table: "AppFileProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_ManagerUserId",
                table: "AppFileProjects");

            migrationBuilder.DropIndex(
                name: "IX_AppFileProjects_AssginUserId",
                table: "AppFileProjects");

            migrationBuilder.DropIndex(
                name: "IX_AppFileProjects_ManagerUserId",
                table: "AppFileProjects");

            migrationBuilder.AlterColumn<int>(
                name: "ManagerUserId",
                table: "AppFileProjects",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssginUserId",
                table: "AppFileProjects",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssginUserId1",
                table: "AppFileProjects",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerUserId1",
                table: "AppFileProjects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_AssginUserId1",
                table: "AppFileProjects",
                column: "AssginUserId1");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_ManagerUserId1",
                table: "AppFileProjects",
                column: "ManagerUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_AssginUserId1",
                table: "AppFileProjects",
                column: "AssginUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AspNetUsers_ManagerUserId1",
                table: "AppFileProjects",
                column: "ManagerUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
