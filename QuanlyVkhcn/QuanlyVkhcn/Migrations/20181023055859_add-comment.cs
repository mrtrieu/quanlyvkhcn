﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class addcomment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "AppFileProjects",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FileUploasdId",
                table: "AppFileProjects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "AppFileProjects");

            migrationBuilder.DropColumn(
                name: "FileUploasdId",
                table: "AppFileProjects");
        }
    }
}
