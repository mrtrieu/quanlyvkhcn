﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class mapidprojectfileproject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileDetails_AppFileProjects_FileProjectId",
                table: "AppFileDetails");

            migrationBuilder.DropIndex(
                name: "IX_AppFileDetails_FileProjectId",
                table: "AppFileDetails");

            migrationBuilder.DropColumn(
                name: "FileProjectId",
                table: "AppFileDetails");

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "AppFileProjects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_ProjectId",
                table: "AppFileProjects",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AppProjects_ProjectId",
                table: "AppFileProjects",
                column: "ProjectId",
                principalTable: "AppProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AppProjects_ProjectId",
                table: "AppFileProjects");

            migrationBuilder.DropIndex(
                name: "IX_AppFileProjects_ProjectId",
                table: "AppFileProjects");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "AppFileProjects");

            migrationBuilder.AddColumn<int>(
                name: "FileProjectId",
                table: "AppFileDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppFileDetails_FileProjectId",
                table: "AppFileDetails",
                column: "FileProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileDetails_AppFileProjects_FileProjectId",
                table: "AppFileDetails",
                column: "FileProjectId",
                principalTable: "AppFileProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
