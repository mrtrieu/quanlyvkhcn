﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class addfileprojectfiledetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_AppProducts_ParentId",
                table: "Projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Projects",
                table: "Projects");

            migrationBuilder.RenameTable(
                name: "Projects",
                newName: "AppProjects");

            migrationBuilder.RenameIndex(
                name: "IX_Projects_ParentId",
                table: "AppProjects",
                newName: "IX_AppProjects_ParentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AppProjects",
                table: "AppProjects",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "AppFileProjects",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    FileUploadId = table.Column<int>(nullable: false),
                    AssginUserId = table.Column<string>(nullable: true),
                    ManagerUserId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsLimitUser = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFileProjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppFileProjects_AspNetUsers_AssginUserId",
                        column: x => x.AssginUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppFileProjects_AspNetUsers_ManagerUserId",
                        column: x => x.ManagerUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AppFileDetails",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true),
                    Path = table.Column<string>(maxLength: 500, nullable: true),
                    Detail = table.Column<string>(maxLength: 500, nullable: true),
                    TargetType = table.Column<string>(maxLength: 20, nullable: true),
                    TargetId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    FileProjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFileDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppFileDetails_AppFileProjects_FileProjectId",
                        column: x => x.FileProjectId,
                        principalTable: "AppFileProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AppFileDetails_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppFileDetails_FileProjectId",
                table: "AppFileDetails",
                column: "FileProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileDetails_TargetId",
                table: "AppFileDetails",
                column: "TargetId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileDetails_UserId",
                table: "AppFileDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_AssginUserId",
                table: "AppFileProjects",
                column: "AssginUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_FileUploadId",
                table: "AppFileProjects",
                column: "FileUploadId");

            migrationBuilder.CreateIndex(
                name: "IX_AppFileProjects_ManagerUserId",
                table: "AppFileProjects",
                column: "ManagerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppProjects_AppProducts_ParentId",
                table: "AppProjects",
                column: "ParentId",
                principalTable: "AppProducts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AppFileDetails_FileUploadId",
                table: "AppFileProjects",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppProjects_AppProducts_ParentId",
                table: "AppProjects");

            migrationBuilder.DropForeignKey(
                name: "FK_AppFileDetails_AppFileProjects_FileProjectId",
                table: "AppFileDetails");

            migrationBuilder.DropTable(
                name: "AppFileProjects");

            migrationBuilder.DropTable(
                name: "AppFileDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AppProjects",
                table: "AppProjects");

            migrationBuilder.RenameTable(
                name: "AppProjects",
                newName: "Projects");

            migrationBuilder.RenameIndex(
                name: "IX_AppProjects_ParentId",
                table: "Projects",
                newName: "IX_Projects_ParentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Projects",
                table: "Projects",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_AppProducts_ParentId",
                table: "Projects",
                column: "ParentId",
                principalTable: "AppProducts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
