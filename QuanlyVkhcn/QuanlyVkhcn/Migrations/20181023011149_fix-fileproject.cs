﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class fixfileproject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AppFileDetails_FileUploadId",
                table: "AppFileProjects");

            migrationBuilder.AlterColumn<int>(
                name: "FileUploadId",
                table: "AppFileProjects",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AppFileDetails_FileUploadId",
                table: "AppFileProjects",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppFileProjects_AppFileDetails_FileUploadId",
                table: "AppFileProjects");

            migrationBuilder.AlterColumn<int>(
                name: "FileUploadId",
                table: "AppFileProjects",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AppFileProjects_AppFileDetails_FileUploadId",
                table: "AppFileProjects",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
