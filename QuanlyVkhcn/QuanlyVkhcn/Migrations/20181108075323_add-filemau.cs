﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class addfilemau : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppFilemau",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Loaifile = table.Column<string>(nullable: true),
                    Tenfile = table.Column<string>(nullable: true),
                    Ngaytao = table.Column<DateTime>(nullable: false),
                    Nguoitao = table.Column<string>(nullable: true),
                    FileUploadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFilemau", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppFilemau_AppFileDetails_FileUploadId",
                        column: x => x.FileUploadId,
                        principalTable: "AppFileDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppFilemau_FileUploadId",
                table: "AppFilemau",
                column: "FileUploadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppFilemau");
        }
    }
}
