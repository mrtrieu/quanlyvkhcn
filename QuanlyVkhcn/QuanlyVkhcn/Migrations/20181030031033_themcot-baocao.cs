﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class themcotbaocao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FilePublicId",
                table: "AppBaocao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppBaocao_FilePublicId",
                table: "AppBaocao",
                column: "FilePublicId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBaocao_AppFileDetails_FilePublicId",
                table: "AppBaocao",
                column: "FilePublicId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBaocao_AppFileDetails_FilePublicId",
                table: "AppBaocao");

            migrationBuilder.DropIndex(
                name: "IX_AppBaocao_FilePublicId",
                table: "AppBaocao");

            migrationBuilder.DropColumn(
                name: "FilePublicId",
                table: "AppBaocao");
        }
    }
}
