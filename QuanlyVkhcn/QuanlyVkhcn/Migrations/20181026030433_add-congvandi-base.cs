﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class addcongvandibase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "File",
                table: "AppCongvandi");

            migrationBuilder.DropColumn(
                name: "File",
                table: "AppCongvanden");

            migrationBuilder.DropColumn(
                name: "File",
                table: "AppBaocao");

            migrationBuilder.AddColumn<int>(
                name: "FileUploadId",
                table: "AppCongvandi",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FileUploadId",
                table: "AppCongvanden",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FileUploadId",
                table: "AppBaocao",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppCongvandi_FileUploadId",
                table: "AppCongvandi",
                column: "FileUploadId");

            migrationBuilder.CreateIndex(
                name: "IX_AppCongvanden_FileUploadId",
                table: "AppCongvanden",
                column: "FileUploadId");

            migrationBuilder.CreateIndex(
                name: "IX_AppBaocao_FileUploadId",
                table: "AppBaocao",
                column: "FileUploadId");

            migrationBuilder.AddForeignKey(
                name: "FK_AppBaocao_AppFileDetails_FileUploadId",
                table: "AppBaocao",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppCongvanden_AppFileDetails_FileUploadId",
                table: "AppCongvanden",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppCongvandi_AppFileDetails_FileUploadId",
                table: "AppCongvandi",
                column: "FileUploadId",
                principalTable: "AppFileDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppBaocao_AppFileDetails_FileUploadId",
                table: "AppBaocao");

            migrationBuilder.DropForeignKey(
                name: "FK_AppCongvanden_AppFileDetails_FileUploadId",
                table: "AppCongvanden");

            migrationBuilder.DropForeignKey(
                name: "FK_AppCongvandi_AppFileDetails_FileUploadId",
                table: "AppCongvandi");

            migrationBuilder.DropIndex(
                name: "IX_AppCongvandi_FileUploadId",
                table: "AppCongvandi");

            migrationBuilder.DropIndex(
                name: "IX_AppCongvanden_FileUploadId",
                table: "AppCongvanden");

            migrationBuilder.DropIndex(
                name: "IX_AppBaocao_FileUploadId",
                table: "AppBaocao");

            migrationBuilder.DropColumn(
                name: "FileUploadId",
                table: "AppCongvandi");

            migrationBuilder.DropColumn(
                name: "FileUploadId",
                table: "AppCongvanden");

            migrationBuilder.DropColumn(
                name: "FileUploadId",
                table: "AppBaocao");

            migrationBuilder.AddColumn<string>(
                name: "File",
                table: "AppCongvandi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "File",
                table: "AppCongvanden",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "File",
                table: "AppBaocao",
                nullable: true);
        }
    }
}
