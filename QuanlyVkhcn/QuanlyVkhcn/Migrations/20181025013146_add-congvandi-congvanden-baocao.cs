﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanlyVkhcn.Migrations
{
    public partial class addcongvandicongvandenbaocao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppBaocao",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Loaibc = table.Column<string>(nullable: true),
                    Tenbc = table.Column<string>(nullable: true),
                    Ngaybc = table.Column<string>(nullable: true),
                    Nguoibc = table.Column<string>(nullable: true),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppBaocao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppCongvanden",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Loaicv = table.Column<string>(nullable: true),
                    Socv = table.Column<string>(nullable: true),
                    Ngaybh = table.Column<string>(nullable: true),
                    Noidungcv = table.Column<string>(nullable: true),
                    CQbh = table.Column<string>(nullable: true),
                    Ngaynhan = table.Column<string>(nullable: true),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppCongvanden", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppCongvandi",
                columns: table => new
                {
                    CreatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedBy = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Loaicv = table.Column<string>(nullable: true),
                    Socv = table.Column<string>(nullable: true),
                    Ngaybh = table.Column<string>(nullable: true),
                    Noidungcv = table.Column<string>(nullable: true),
                    Nguoiky = table.Column<string>(nullable: true),
                    Noinhan = table.Column<string>(nullable: true),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppCongvandi", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppBaocao");

            migrationBuilder.DropTable(
                name: "AppCongvanden");

            migrationBuilder.DropTable(
                name: "AppCongvandi");
        }
    }
}
