
import { AppPage } from './app.po';

describe('QuanlyVkhcn App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display application title: QuanlyVkhcn', () => {
    page.navigateTo();
    expect(page.getAppTitle()).toEqual('QuanlyVkhcn');
  });
});
