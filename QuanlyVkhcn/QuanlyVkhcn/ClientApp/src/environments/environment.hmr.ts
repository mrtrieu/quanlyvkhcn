
export const environment = {
    production: false,
    baseUrl: 'http://localhost:60266', // Change this to the address of your backend API if different from frontend address
    loginUrl: '/Login',
    googleDocUrl: 'http://localhost:60266/api/fileproject/download/',
    hmr: true
};
