export const environment = {
    production: true,
    baseUrl: null, // Change this to the address of your backend API if different from frontend address
    loginUrl: '/Login',
    googleDocUrl: 'http://103.252.252.131/api/fileproject/download/',
    hmr: false
};
