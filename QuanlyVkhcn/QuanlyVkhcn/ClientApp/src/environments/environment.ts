
export const environment = {
  production: false,
  baseUrl: 'http://localhost:60266', // Change this to the address of your backend API if different from frontend address
  loginUrl: '/Login',
  googleDocUrl: 'http://localhost:60266/api/fileproject/download/',
  hmr: false
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
