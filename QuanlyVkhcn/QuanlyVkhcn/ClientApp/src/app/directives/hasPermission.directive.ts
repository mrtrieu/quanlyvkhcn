import { AccountService } from './../services/account.service';
import { ElementRef, TemplateRef, ViewContainerRef, Input, Directive, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[hasPermission]'
})
export class HasPermissionDirective implements OnInit {
    private permissions = [];
    isHidden: any = true;
    private logicalOp = false;

    constructor(
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private userService: AuthService,
        private accountService: AccountService,
    ) {
    }

    ngOnInit() {
        this.userService.getLoginStatusEvent().subscribe(user => {
            this.updateView();
        });
    }
    @Input()
    set hasPermission(val) {
        this.permissions = val;
        this.updateView();
    }
    @Input()
    set hasPermissionAll(permop) {
        this.logicalOp = permop;
        this.updateView();
    }

    private updateView() {
        if (this.checkPermission()) {
            if (this.isHidden) {
                this.viewContainer.createEmbeddedView(this.templateRef);
                this.isHidden = false;
            }
        } else {
            this.isHidden = true;
            this.viewContainer.clear();
        }
    }

    private checkPermission() {
        return this.accountService.userHasListPermission(this.permissions, this.logicalOp);
    }
}
