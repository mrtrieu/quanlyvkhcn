
export type PermissionNames =
    'View Users' | 'Manage Users' |
    'View Roles' | 'Manage Roles' | 'Assign Roles' |
    'Xem file dự án' | 'Xóa File dự án' | 'Duyệt file dự án' | 'Cập nhật file dự án';

export type PermissionValues =
    'users.view' | 'users.manage' |
    'roles.view' | 'roles.manage' | 'roles.assign' |
    'fileProject.filemanage' | 'fileProject.view' | 'fileProject.edit' | 'fileProject.approve' | 'fileProject.addFile' |
    'news.view' | 'news.edit' |
    'congvan.view' | 'congvan.edit';
export class Permission {

    public static readonly viewUsersPermission: PermissionValues = 'users.view';
    public static readonly manageUsersPermission: PermissionValues = 'users.manage';

    public static readonly viewRolesPermission: PermissionValues = 'roles.view';
    public static readonly manageRolesPermission: PermissionValues = 'roles.manage';
    public static readonly assignRolesPermission: PermissionValues = 'roles.assign';

    public static readonly viewFileProjectPermission: PermissionValues = 'fileProject.view';
    public static readonly viewFileManagePermission: PermissionValues = 'fileProject.filemanage';
    public static readonly viewFileEditPermission: PermissionValues = 'fileProject.edit';
    public static readonly viewFileApprovePermission: PermissionValues = 'fileProject.approve';
    public static readonly viewFileAddFilePermission: PermissionValues = 'fileProject.addFile';

    public static readonly NewsViewPermission: PermissionValues = 'news.view';
    public static readonly NewsEditPermission: PermissionValues = 'news.edit';

    public static readonly CongvanViewPermission: PermissionValues = 'congvan.view';
    public static readonly CongvanEditPermission: PermissionValues = 'congvan.edit';

    constructor(name?: PermissionNames, value?: PermissionValues, groupName?: string, description?: string) {
        this.name = name;
        this.value = value;
        this.groupName = groupName;
        this.description = description;
    }

    public name: PermissionNames;
    public value: PermissionValues;
    public groupName: string;
    public description: string;
}
