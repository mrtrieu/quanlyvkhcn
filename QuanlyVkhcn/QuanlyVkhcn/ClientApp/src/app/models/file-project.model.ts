import { FileProjectModel } from './file-project.model';
import { User } from './user.model';

export interface FileProjectModel {
    id?: number;
    name?: string;
    description?: string;
    comment?: string;
    fileUploadId?: number;
    fileUpload?: FileDetailModel;
    oldFileUpload?: FileDetailModel;
    assginUserId?: string;
    managerUserId?: string;
    assginUser?: User;
    managerUser?: User;
    projectId?: number;
    project?: FileProjectModel;
    status?: 'Init' | 'Waiting' | 'Decline' | 'Approve' | 'End' | number;
    IsLimitUser?: boolean;
    createdDate?: Date;
}

export const FileProjectStatus = {
    Init: 0,
    Waiting: 1,
    Decline: 2,
    Approve: 3,
    End: 4,
};

export interface FileDetailModel {
    id?: number;
    name?: string;
    path?: string;
    detail?: string;
    targetType?: string;
    targetId?: string;
    createdDate?: string;
}

export interface ProjectViewModel {
    id?: number;
    name?: string;
    description?: string;

}

