import { FileDetailModel } from './file-project.model';

export class CongvandiDetail {
    id?: string;
    loaicv?: string;
    socv?: string;
    ngaybh?: string | Date;
    noidungcv?: string;
    nguoiky?: string;
    noinhan?: string;
    fileUpload?: FileDetailModel;
    fileUploadId?: Number;
}




