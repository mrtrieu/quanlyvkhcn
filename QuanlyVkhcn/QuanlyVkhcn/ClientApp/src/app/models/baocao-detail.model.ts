import { FileDetailModel } from './file-project.model';

export class BaocaoDetail {
    id?: string;
    loaibc?: string;
    tenbc?: string;
    ngaybc?: Date;
    nguoibc?: string;
    fileUpload?: FileDetailModel;
    fileUploadId?: Number;
    filePublic?: FileDetailModel;
    filePublicId?: Number;
}




