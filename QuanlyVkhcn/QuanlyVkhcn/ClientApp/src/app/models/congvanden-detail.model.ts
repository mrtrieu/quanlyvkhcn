import { FileDetailModel } from './file-project.model';

export class CongvandenDetail {
    id?: string;
    loaicv?: string;
    socv?: string;
    ngaybh?: string | Date;
    ngaybc?: Date;
    noidungcv?: string;
    cqbh?: string;
    ngaynhan?: string;
    fileUpload?: FileDetailModel;
    fileUploadId?: Number;
}



