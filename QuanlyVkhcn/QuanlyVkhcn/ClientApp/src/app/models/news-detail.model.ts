export class NewsDetail {
    id?: string;
    title_VN?: string;
    title_EN?: string;
    content_EN?: string;
    content_VN?: string;
    image_EN?: string;
    image_VN?: string;
    isView_VN?: string;
    isView_EN?: string;
    category?: string;
}




