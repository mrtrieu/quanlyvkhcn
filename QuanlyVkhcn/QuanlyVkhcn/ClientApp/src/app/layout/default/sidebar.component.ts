import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SideBarComponent implements OnInit {

    appTitle = 'VKHCN';
    appLogo = require('../../assets/images/logo.jpg');
    constructor(
        public router: Router
    ) {

    }

    ngOnInit() {
        // const sidebar: any = require('../../assets/scripts/sidebar.js');
    }

    toggleSideBar($event: MouseEvent) {
        $event.preventDefault();
        $('.app').toggleClass('is-collapsed');
    }

    toggleDropDown($event: MouseEvent) {
        const $this = $($event.currentTarget);
        if ($this.parent().hasClass('open')) {
            $this
                .parent()
                .children('.dropdown-menu')
                .slideUp(200, () => {
                    $this.parent().removeClass('open');
                });
        } else {
            $this
                .parent()
                .parent()
                .children('li.open')
                .children('.dropdown-menu')
                .slideUp(200);

            $this
                .parent()
                .parent()
                .children('li.open')
                .children('a')
                .removeClass('open');

            $this
                .parent()
                .parent()
                .children('li.open')
                .removeClass('open');

            $this
                .parent()
                .children('.dropdown-menu')
                .slideDown(200, () => {
                    $this.parent().addClass('open');
                });
        }
    }

    onSwipeLeft(id, $event) {
        console.log($event);
    }

    onSwipeRight($event) {
        console.log($event);
    }
}
