import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  constructor(private authService: AuthService) { }
  accountImage = require('../../assets/images/account.png');
  ngOnInit() {
  }


  logout() {
    this.authService.logout();
    this.authService.redirectLogoutUser();
  }



  toggleSideBar($event: MouseEvent) {
    $event.preventDefault();
    $('.app').toggleClass('is-collapsed');
}

  get userName(): string {
    return this.authService.currentUser ? this.authService.currentUser.userName : '';
  }


  get fullName(): string {
    return this.authService.currentUser ? this.authService.currentUser.fullName : '';
  }
}
