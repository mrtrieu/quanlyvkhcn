import { HasPermissionDirective } from './../directives/hasPermission.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateLanguageLoader, AppTranslationService } from 'src/app/services/app-translation.service';
import { SharedModule } from '../components/controls/shared.module';
import { DefaultComponent } from './default/default.component';
import { SideBarComponent } from './default/sidebar.component';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutEmptyComponent } from './layout-empty/layout-empty.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgZorroAntdModule,
        ModalModule,
        SharedModule,
        AppRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useClass: TranslateLanguageLoader
            }
        }),
    ],
    providers: [
        AppTranslationService,
    ],
    declarations: [
        DefaultComponent,
        SideBarComponent,
        LayoutEmptyComponent,
    ], exports: [
        DefaultComponent,
        SideBarComponent,
        LayoutEmptyComponent,
    ]
})
export class LayoutModule { }
