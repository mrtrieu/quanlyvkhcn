import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { BaocaoComponent } from './components/baocao/baocao.component';
import { CongvandenComponent } from './components/congvanden/congvanden.component';
import { CongvandiComponent } from './components/congvandi/congvandi.component';
import { FileProjectsComponent } from './components/file-projects/file-projects.component';
import { FilemauComponent } from './components/filemau/filemau.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SettingsComponent } from './components/settings/settings.component';
import { Permission } from './models/permission.model';
import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard], data: { title: 'Home' } },
  { path: 'login', component: LoginComponent, data: { title: 'Login' } },
  {
    path: 'news',
    loadChildren: './components/news/news.module#NewsModule',
    canLoad: [AuthGuard],
    data: { title: 'News' }
  },
  {
    path: 'congvandi', component: CongvandiComponent, canActivate: [AuthGuard], data: {
      title: 'Congvandi',
      auth: Permission.CongvanViewPermission
    }
  },
  {
    path: 'congvanden', component: CongvandenComponent, canActivate: [AuthGuard], data: {
      title: 'Congvanden',
      auth: Permission.CongvanViewPermission
    }
  },
  {
    path: 'baocao', component: BaocaoComponent, canActivate: [AuthGuard], data: {
      title: 'Baocao',
      auth: Permission.CongvanViewPermission
    }
  },
  {
    path: 'filemau', component: FilemauComponent, canActivate: [AuthGuard], data: {
      title: 'Filemau',
      auth: Permission.CongvanViewPermission
    }
  },
  {
    path: 'project-files', component: FileProjectsComponent, canActivate: [AuthGuard], data: {
      title: 'File Project',
      auth: Permission.viewFileProjectPermission
    }
  },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], data: { title: 'Settings' } },
  { path: 'about', component: AboutComponent, data: { title: 'About Us' } },
  { path: 'home', redirectTo: '/', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent, data: { title: 'Page Not Found' } }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthService, AuthGuard]
})
export class AppRoutingModule { }
