import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'projectStatus'
})
export class ProjectStatusPipe implements PipeTransform {

  transform(value: string | Number, args?: any): any {
    if (value === null || value === undefined) { return null; }
    const data = value.toString();
    let name = '';
    let color = '';
    switch (data) {
      case '0':
      case 'Init':
        name = 'Khởi tạo';
        color = 'secondary';
        break;
      case '1':
      case 'Waiting':
        name = 'Chờ duyệt';
        color = 'primary';
        break;
      case '2':
      case 'Decline':
        name = 'Cần chỉnh sửa';
        color = 'danger';
        break;
      case '3':
      case 'Approve':
        name = 'Phê duyệt';
        color = 'success';
        break;
      case '4':
      case 'End':
        name = 'Kết thúc';
        color = 'info';
        break;
    }
    return `<span class="badge badge-${color}">${name}</span>`;
  }

}
