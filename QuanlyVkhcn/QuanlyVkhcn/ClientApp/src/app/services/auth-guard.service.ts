import { AlertService, MessageSeverity } from 'src/app/services/alert.service';


import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild,
    NavigationExtras,
    CanLoad,
    Route
} from '@angular/router';
import { AuthService } from './auth.service';
import { AccountService } from './account.service';
import { AlertDialog } from './alert.service';


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
    constructor(private authService: AuthService, private router: Router, private accountService: AccountService,
        private dialog: AlertService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const url: string = state.url;
        return this.checkLogin(url, route.data.auth ? [...route.data.auth] : undefined);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    canLoad(route: Route): boolean {
        const url = `/${route.path}`;
        return this.checkLogin(url, route.data.auth ? [...route.data.auth] : undefined);
    }

    checkLogin(url: string, ...permissions: any[]): boolean {
        if (!this.authService.isLoggedIn) {
            this.authService.loginRedirectUrl = url;
            this.router.navigate(['/login']);
            return false;
        }
        if (permissions && permissions[0]) {
            const check = permissions.every(o => o && this.accountService.userHasPermission(o));
            if (!check) {
                this.dialog.showMessage('Error', 'Không thể truy cập', MessageSeverity.error);
                return false;
            }
        }

        console.log('allow login');
        return true;
    }
}
