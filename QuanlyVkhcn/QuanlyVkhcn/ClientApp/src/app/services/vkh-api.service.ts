import { MessageSeverity, AlertService } from 'src/app/services/alert.service';
import { FileProjectModel, FileDetailModel, ProjectViewModel } from './../models/file-project.model';
import { AuthService } from './auth.service';

import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { NewsDetail } from '../models/news-detail.model';
import { CongvandiDetail } from '../models/congvandi-detail.model';
import { CongvandenDetail } from '../models/congvanden-detail.model';
import { BaocaoDetail } from '../models/baocao-detail.model';
import { FilemauDetail } from '../models/filemau-detail.model';
import { ConfigurationService } from './configuration.service';
import { EndpointFactory } from './endpoint-factory.service';



@Injectable()
export class VkhApiService {
    constructor(
        private authService: AuthService,
        private http: HttpClient,
        private configurations: ConfigurationService,
        private alertDialog: AlertService,
    ) {
    }

    getRequestHeaders(): { headers: HttpHeaders } {
        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + this.authService.accessToken,
            'Content-Type': 'application/json',
            'Accept': `application/vnd.iman.v${EndpointFactory.apiVersion}+json, application/json, text/plain, */*`,
            'App-Version': ConfigurationService.appVersion
        });

        return { headers: headers };
    }
    getNews(): Observable<Array<NewsDetail>> {
        return this.http.get<Array<NewsDetail>>(`${this.configurations.baseUrl}/api/news`, this.getRequestHeaders());
    }

    getNewsById(id: string): Observable<NewsDetail> {
        return this.http.get<NewsDetail>(`${this.configurations.baseUrl}/api/news/${id}`, this.getRequestHeaders());
    }

    addNews(news: NewsDetail) {
        return this.http.post(`${this.configurations.baseUrl}/api/news`, news, this.getRequestHeaders());
    }

    updateNews(news: NewsDetail) {
        return this.http.put(`${this.configurations.baseUrl}/api/news/${news.id}`, news, this.getRequestHeaders());
    }

    deleteNews(id: string) {
        return this.http.delete(`${this.configurations.baseUrl}/api/news/${id}`, this.getRequestHeaders());
    }
    getCongvandi(): Observable<Array<CongvandiDetail>> {
        return this.http.get<Array<CongvandiDetail>>(`${this.configurations.baseUrl}/api/congvandi`, this.getRequestHeaders());
    }

    addCongvandi(congvandi: CongvandiDetail) {
        return this.http.post(`${this.configurations.baseUrl}/api/congvandi`, congvandi, this.getRequestHeaders());
    }

    updateCongvandi(congvandi: CongvandiDetail) {
        return this.http.put(`${this.configurations.baseUrl}/api/congvandi/${congvandi.id}`, congvandi, this.getRequestHeaders());
    }

    deleteCongvandi(id: string) {
        return this.http.delete(`${this.configurations.baseUrl}/api/congvandi/${id}`, this.getRequestHeaders());
    }

    getCongvanden(): Observable<Array<CongvandenDetail>> {
        return this.http.get<Array<CongvandenDetail>>(`${this.configurations.baseUrl}/api/congvanden`, this.getRequestHeaders());
    }

    addCongvanden(congvanden: CongvandenDetail) {
        return this.http.post(`${this.configurations.baseUrl}/api/congvanden`, congvanden, this.getRequestHeaders());
    }

    updateCongvanden(congvanden: CongvandenDetail) {
        return this.http.put(`${this.configurations.baseUrl}/api/congvanden/${congvanden.id}`, congvanden, this.getRequestHeaders());
    }

    deleteCongvanden(id: string) {
        return this.http.delete(`${this.configurations.baseUrl}/api/congvanden/${id}`, this.getRequestHeaders());
    }

    getBaocao(): Observable<Array<BaocaoDetail>> {
        return this.http.get<Array<BaocaoDetail>>(`${this.configurations.baseUrl}/api/baocao`, this.getRequestHeaders());
    }

    addBaocao(baocao: BaocaoDetail) {
        return this.http.post(`${this.configurations.baseUrl}/api/baocao`, baocao, this.getRequestHeaders());
    }

    updateBaocao(baocao: BaocaoDetail) {
        return this.http.put(`${this.configurations.baseUrl}/api/baocao/${baocao.id}`, baocao, this.getRequestHeaders());
    }

    deleteBaocao(id: string) {
        return this.http.delete(`${this.configurations.baseUrl}/api/baocao/${id}`, this.getRequestHeaders());
    }

    getFilemau(): Observable<Array<FilemauDetail>> {
        return this.http.get<Array<FilemauDetail>>(`${this.configurations.baseUrl}/api/filemau`, this.getRequestHeaders());
    }

    addFilemau(filemau: FilemauDetail) {
        return this.http.post(`${this.configurations.baseUrl}/api/filemau`, filemau, this.getRequestHeaders());
    }

    updateFilemau(filemau: FilemauDetail) {
        return this.http.put(`${this.configurations.baseUrl}/api/filemau/${filemau.id}`, filemau, this.getRequestHeaders());
    }

    deleteFilemau(id: string) {
        return this.http.delete(`${this.configurations.baseUrl}/api/filemau/${id}`, this.getRequestHeaders());
    }

    getProjects(page?: Number, limit?: Number) {
        return this.http.get<Array<ProjectViewModel>>(`${this.configurations.baseUrl}/api/project`, this.getRequestHeaders());
    }

    addProject(fp: ProjectViewModel) {
        return this.http.post(`${this.configurations.baseUrl}/api/project`, fp, this.getRequestHeaders());
    }

    deleteProject(id: Number) {
        return this.http.delete(`${this.configurations.baseUrl}/api/project/${id}`, this.getRequestHeaders());
    }

    updateProject(fp: ProjectViewModel) {
        return this.http.put(`${this.configurations.baseUrl}/api/project/${fp.id}`, fp, this.getRequestHeaders());
    }

    getFileProjects(page?: Number, limit?: Number) {
        return this.http.get<Array<FileProjectModel>>(`${this.configurations.baseUrl}/api/fileproject`, this.getRequestHeaders());
    }

    getFileProjectsByProjectId(id: Number, page?: Number, limit?: Number) {
        return this.http.get<Array<FileProjectModel>>(
            `${this.configurations.baseUrl}/api/fileproject/project/${id}`,
            this.getRequestHeaders());
    }

    addFileProject(fp: FileProjectModel) {
        return this.http.post(`${this.configurations.baseUrl}/api/fileproject`, fp, this.getRequestHeaders());
    }

    deleteFileProject(id: Number) {
        return this.http.delete(`${this.configurations.baseUrl}/api/fileproject/${id}`, this.getRequestHeaders());
    }

    updateFileProject(fp: FileProjectModel) {
        return this.http.put(`${this.configurations.baseUrl}/api/fileproject/${fp.id}`, fp, this.getRequestHeaders());
    }


    updateFileDetail(file: FileDetailModel) {
        return this.http.put(`${this.configurations.baseUrl}/api/fileproject/file/${file.id}`, file, this.getRequestHeaders());
    }

    uploadFile(file, fileType: 'congvanden' | 'congvandi' | 'baocao' | 'fileproject' | 'filetemplate',
        targetId?: string, detail?: string): Observable<any> {
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('targetType', fileType);
        if (targetId) {
            formData.append('targetId', targetId);
        }
        if (detail) {
            formData.append('detail', detail);
        }
        const uploadReq = new HttpRequest('POST', `${this.configurations.baseUrl}/api/fileproject/upload`, formData, {
            reportProgress: true,
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.authService.accessToken,
                'Accept': `application/vnd.iman.v${EndpointFactory.apiVersion}+json, application/json, text/plain, */*`,
                'App-Version': ConfigurationService.appVersion
            })
        });
        return this.http.request(uploadReq);
    }





    getFileuploadByTargetId(targetId): Observable<FileDetailModel> {
        return this.http.get(`${this.configurations.baseUrl}/api/fileproject/file/type/${targetId}`);
    }

    downloadFile(id, filename) {
        const downloadRequest = new HttpRequest('GET', `${this.configurations.baseUrl}/api/fileproject/download/${id}`, {}, {
            responseType: 'blob',
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.authService.accessToken,
                'App-Version': ConfigurationService.appVersion
            })
        });
        this.http.request(downloadRequest).subscribe((resp: any) => {
            if (resp.type === 4 && resp.headers) {
                this.createAndDownloadBlobFile(resp.body, {}, filename);
            }
        }, error => {
            this.alertDialog.showMessage('Lỗi', 'Không tìm thấy file', MessageSeverity.error);
        });
    }

    private createAndDownloadBlobFile(res, options, filename) {
        const blob = new Blob([res], options);
        if (navigator.msSaveBlob) {
            // IE 10+
            navigator.msSaveBlob(blob, res.filename);
        } else {
            const link = document.createElement('a');
            // Browsers that support HTML5 download attribute
            if (link.download !== undefined) {
                const url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
}
