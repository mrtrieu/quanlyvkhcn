

import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { en_US, NgZorroAntdModule, NZ_I18N } from 'ng-zorro-antd';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ToastaModule } from 'ngx-toasta';
import { AppErrorHandler } from './app-error.handler';
import { AppRoutingModule } from './app-routing.module';
import { AboutComponent } from './components/about/about.component';
import { AppComponent } from './components/app.component';
import { BaocaoComponent } from './components/baocao/baocao.component';
import { ChatzaloComponent } from './components/chatzalo/chatzalo.component';
import { CongvandenComponent } from './components/congvanden/congvanden.component';
import { CongvandiComponent } from './components/congvandi/congvandi.component';
import { DocViewComponent } from './components/controls/doc-view/doc-view.component';
import { NotificationsViewerComponent } from './components/controls/notifications-viewer.component';
import { RoleEditorComponent } from './components/controls/role-editor.component';
import { RolesManagementComponent } from './components/controls/roles-management.component';
import { SharedModule } from './components/controls/shared.module';
import { UserInfoComponent } from './components/controls/user-info.component';
import { UserPreferencesComponent } from './components/controls/user-preferences.component';
import { UsersManagementComponent } from './components/controls/users-management.component';
import { CustomersComponent } from './components/customers/customers.component';
import { FileProjectsComponent } from './components/file-projects/file-projects.component';
import { FilemauComponent } from './components/filemau/filemau.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewsModule } from './components/news/news.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProductsComponent } from './components/products/products.component';
import { SettingsComponent } from './components/settings/settings.component';
import { AutofocusDirective } from './directives/autofocus.directive';
import { BootstrapTabDirective } from './directives/bootstrap-tab.directive';
import { EqualValidator } from './directives/equal-validator.directive';
import { LastElementDirective } from './directives/last-element.directive';
import { GroupByPipe } from './pipes/group-by.pipe';
import { ProjectStatusPipe } from './pipes/project-status.pipe';
import { AccountEndpoint } from './services/account-endpoint.service';
import { AccountService } from './services/account.service';
import { AlertService } from './services/alert.service';
import { AppTitleService } from './services/app-title.service';
import { AppTranslationService, TranslateLanguageLoader } from './services/app-translation.service';
import { ConfigurationService } from './services/configuration.service';
import { EndpointFactory } from './services/endpoint-factory.service';
import { LocalStoreManager } from './services/local-store-manager.service';
import { NotificationEndpoint } from './services/notification-endpoint.service';
import { NotificationService } from './services/notification.service';
import { VkhApiService } from './services/vkh-api.service';
import { LayoutModule } from './layout/layout.module';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLanguageLoader
      }
    }),
    NgxDatatableModule,
    ToastaModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    SharedModule,
    LayoutModule,
    NewsModule,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CustomersComponent,
    ProductsComponent,
    OrdersComponent,
    SettingsComponent,
    UsersManagementComponent, UserInfoComponent, UserPreferencesComponent,
    RolesManagementComponent, RoleEditorComponent,
    AboutComponent,
    NotFoundComponent,
    NotificationsViewerComponent,
    EqualValidator,
    LastElementDirective,
    AutofocusDirective,
    BootstrapTabDirective,
    GroupByPipe,
    FileProjectsComponent,
    ProjectStatusPipe,
    CongvandiComponent,
    CongvandenComponent,
    BaocaoComponent,
    FilemauComponent,
    DocViewComponent,
    ChatzaloComponent,
  ],
  providers: [
    { provide: 'BASE_URL', useFactory: getBaseUrl },
    [{ provide: NZ_I18N, useValue: en_US }],
    { provide: ErrorHandler, useClass: AppErrorHandler },
    AlertService,
    ConfigurationService,
    AppTitleService,
    AppTranslationService,
    NotificationService,
    NotificationEndpoint,
    AccountService,
    AccountEndpoint,
    LocalStoreManager,
    EndpointFactory,
    VkhApiService
  ],
  entryComponents: [DocViewComponent],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {
}




export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
