import { FileDetailModel } from './../../../models/file-project.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NewsDetail } from 'src/app/models/news-detail.model';
import { VkhApiService } from 'src/app/services/vkh-api.service';
import { AlertService } from 'src/app/services/alert.service';
import { FormPage } from '../../controls/form-page';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigurationService } from 'src/app/services/configuration.service';

@Component({
    selector: 'app-news-edit',
    templateUrl: './news-edit.component.html',
    styleUrls: ['./news-edit.component.css']
})
export class NewsEditComponent extends FormPage implements OnInit {
    formResetToggle: boolean;

    isNew = false;
    columns = [
        { name: 'Id' },
        { name: 'title_VN', prop: 'title_VN' },
        { name: 'Content', prop: 'content_VN' }
    ];
    data: Array<NewsDetail>;
    newsForm: FormGroup;
    newsId: string;
    newsData: any;
    editorConfig: AngularEditorConfig;
    baseUrl: string;
    image_VN: string;
    image_EN: string;
    constructor(
        private api: VkhApiService,
        protected alertService: AlertService,
        private router: Router,
        private route: ActivatedRoute,
        private config: ConfigurationService,
        fb: FormBuilder) {
        super();
        this.newsForm = fb.group({
            title_VN: [''],
            title_EN: [''],
            content_EN: [''],
            content_VN: [''],
            category: [''],
            isView_VN: [true],
            isView_EN: [true],
            image_VN: [''],
            image_EN: ['']
        });
        this.baseUrl = config.baseUrl;
        this.editorConfig = {
            editable: true,
            spellcheck: true,
            height: '25rem',
            minHeight: '5rem',
            placeholder: 'Enter text here...',
            translate: 'no',
            uploadUrl: this.baseUrl + '/api/fileproject/upload?targetType=image'
        };
    }

    submitForm = ($event, value) => {
        $event.preventDefault();
        if (this.newsForm.valid) {
            const newObj: NewsDetail = {
                title_EN: this.newsForm.get('title_EN').value,
                title_VN: this.newsForm.get('title_VN').value,
                image_VN: this.newsForm.get('image_VN').value,
                image_EN: this.newsForm.get('image_EN').value,
                content_EN: this.newsForm.get('content_EN').value,
                content_VN: this.newsForm.get('content_VN').value,
                isView_VN: this.newsForm.get('isView_VN').value,
                isView_EN: this.newsForm.get('isView_EN').value,
                category: this.newsForm.get('category').value
            };

            if (this.isNew) {
                this.api.addNews(newObj).subscribe(() => {
                    this.goBack();
                });
            } else {
                newObj.id = this.newsId;
                this.api.updateNews(newObj).subscribe(() => {
                    this.goBack();
                });
            }
        }
    }

    getImageUrl(url: string) {
        if (url.startsWith('http') || url.startsWith('//')) {
            return url;
        }
        return this.baseUrl + url;
    }

    goBack() {
        this.router.navigate(['news']);
    }
    resetForm(e: MouseEvent): void {
        e.preventDefault();
        this.newsForm.reset();

    }

    validateConfirmPassword(): void {
    }


    ngOnInit() {
        this.newsId = this.route.snapshot.params.id;
        console.log('this.route.snapshot: ', this.route.snapshot);
        console.log('this.newsId: ', this.newsId);
        if (!this.newsId) {
            this.isNew = true;
        } else {
            this.load(this.newsId);
        }

    }

    load(id) {
        this.api.getNewsById(id).subscribe(o => {
            if (o) {
                this.newsData = o;
                this.newsForm.setValue({
                    title_VN: this.newsData.title_VN,
                    image_VN: this.newsData.image_VN,
                    content_EN: this.newsData.content_EN,
                    image_EN: this.newsData.image_EN,
                    content_VN: this.newsData.content_VN,
                    isView_VN: this.newsData.isView_VN,
                    isView_EN: this.newsData.isView_EN,
                    title_EN: this.newsData.title_EN || '',
                    category: this.newsData.category || '',
                });
            }
        });
    }

    getUploadPath(file: FileDetailModel, controlName: string) {
        const control = this.newsForm.get(controlName);
        if (control) {
            const path = file.path.substring(file.path.indexOf('wwwroot') + 7, file.path.length);
            control.setValue(path.replace(/\\/g, '/'));
        }
    }

}
