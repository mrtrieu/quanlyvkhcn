import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news.component';
import { AuthGuard } from 'src/app/services/auth-guard.service';
import { NewsEditComponent } from './news-edit/news-edit.component';

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'edit', component: NewsEditComponent, canActivate: [AuthGuard] },
  { path: 'edit/:id', component: NewsEditComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }
