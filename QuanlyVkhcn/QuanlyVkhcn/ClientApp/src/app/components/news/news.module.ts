import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SharedModule } from '../controls/shared.module';
import { TranslateLanguageLoader, AppTranslationService } from 'src/app/services/app-translation.service';

import { AngularEditorModule } from '@kolkov/angular-editor';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NewsRoutingModule,
    NgxDatatableModule,
    AngularEditorModule,
    ModalModule,
    SharedModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useClass: TranslateLanguageLoader
      }
    }),
  ],
  providers: [
    AppTranslationService,
  ],
  declarations: [
    NewsComponent,
    NewsEditComponent,
  ]
})
export class NewsModule { }
