import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsDetail } from 'src/app/models/news-detail.model';
import { VkhApiService } from 'src/app/services/vkh-api.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  formResetToggle: boolean;

  taskEdit: NewsDetail = {};

  columns = [
    { name: 'Id', width: 60 },
    { name: 'title_VN', prop: 'title_VN' },
  ];
  data: Array<NewsDetail>;

  constructor(private api: VkhApiService, private router: Router) { }


  ngOnInit() {
    this.load();
  }

  load() {
    this.api.getNews().subscribe(o => {
      if (o) {
        this.data = o;
        console.log('this.data : ', this.data);
      }
    });
  }
  add() {
    this.router.navigate(['news', 'edit']);
  }

  view(newsObj) {
    this.router.navigate(['news', 'edit', newsObj.id]);
  }

  delete(newsObj) {
    this.api.deleteNews(newsObj.id).subscribe(o => {
      this.load();
    });
  }
  onSearchChanged($event) {
  }

}
