import { environment } from './../../../../environments/environment.prod';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FileDetailModel } from './../../../models/file-project.model';

@Component({
  selector: 'app-doc-view',
  template: `
    <iframe [src]="iframeUrl" frameborder="0" style="width:100%;height:100%">
    </iframe>
 `,
  styleUrls: ['./doc-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocViewComponent implements OnInit {
  @Input('url')
  url: FileDetailModel;
  iframeUrl;
  constructor(
    private domSanitizer: DomSanitizer,
  ) {
  }
  ngOnInit() {
    const url = `${environment.googleDocUrl}${this.url.id}`;
    this.iframeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
      `https://docs.google.com/gview?url=${url}&embedded=true`);
  }

}

