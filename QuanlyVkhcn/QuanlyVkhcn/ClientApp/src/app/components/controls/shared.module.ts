import { AppUploadComponent } from './app-upload/app-upload.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBoxComponent } from '../controls/search-box.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { HasPermissionDirective } from 'src/app/directives/hasPermission.directive';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgZorroAntdModule,
        NgxDatatableModule,
        ModalModule.forRoot(),
        TranslateModule.forRoot({
        }),
    ],
    declarations: [
        SearchBoxComponent,
        AppUploadComponent,
        HasPermissionDirective,
    ],
    exports: [
        SearchBoxComponent,
        AppUploadComponent,
        HasPermissionDirective,
    ]
})
export class SharedModule { }
