import { startWith } from 'rxjs/operators';
import { AlertDialog, AlertService } from './../../../services/alert.service';
import { VkhApiService } from './../../../services/vkh-api.service';
import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input, forwardRef } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-upload',
  templateUrl: './app-upload.component.html',
  styleUrls: ['./app-upload.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AppUploadComponent),
      multi: true
    }
  ],
})
export class AppUploadComponent implements OnInit, ControlValueAccessor {

  @Input()
  uploadType: any;
  @Input()
  uploadTargetId: string;
  @Input()
  uploadDetail: string;
  @Input()
  uploadAccept: string;
  @Input()
  delayUpload = false;
  @Input()
  uploadFilterType: string;

  @Input()
  name: string;
  @Input('value')
  value: string;

  @Output()
  uploadFinish: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  @Input() changing: Subject<boolean>;
  protected progress = 0;
  protected message: string;
  protected fileSelected: any;
  protected isSelectedFile: boolean;
  constructor(private api: VkhApiService, private alertService: AlertService) { }
  ngOnInit() {
  }

  upload(files) {
    if (files.length === 0) {
      return;
    }
    this.resetData();
    this.fileSelected = files[0];
    if (!this.delayUpload) {
      this.startUpload();
    } else {

    }
  }
  startUpload(onDone: (value) => void = null) {
    if (!this.fileSelected) {
      onDone(null);
      return;
    }
    if (this.uploadFilterType) {
      const check = this.uploadFilterType.split(',').some(f => {
        return this.fileSelected.type.startWith(f);
      });
      if (check) {
        this.alertService.showMessage('Tập tin không được phép tải lên vui lòng chuyển đổi sang định dạng phù hợp ');
        return;
      }
    }
    this.api.uploadFile(
      this.fileSelected,
      this.uploadType,
      this.uploadTargetId || '',
      this.uploadDetail || ''
    ).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event.type === HttpEventType.Response) {
        if (event.body) {
          const result: any = event.body;
          if (result.ok) {
            this.uploadFinish.next(result.data);
            this.writeValue(result.data);
            if (onDone) {
              onDone(result.data);
            }
          }
          this.message = result.message;
        }
      }
    }, error => this.handleError(error));
  }

  handleError(error) {
    this.progress = 0;
    this.alertService.showMessage('Có lỗi xảy ra vui lòng thử lại sau');
  }

  writeValue(value: any): void {
    this.value = value;
    this.resetData();
    this.onChange(value);
    this.onTouched();
  }

  onChange: any = (value) => { };
  onTouched: any = () => { };
  private resetData() {
    this.progress = 0;
    this.message = '';
    this.fileSelected = null;
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

}
