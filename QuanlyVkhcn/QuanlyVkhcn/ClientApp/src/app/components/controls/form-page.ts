import { FormControl, FormGroup } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
export class FormPage {
    public isLoading = false;
    public formSubmitAttempt = false;
    protected alertService: AlertService;
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }


    isControlValid(control: FormControl | FormGroup, controlName: string): boolean {
        if (controlName) {
            const newcontrol = control.get(controlName);
            control = newcontrol || control as any;
        }
        return (!control.valid && control.touched) || (control.untouched && this.formSubmitAttempt);
    }
    getMessageValidator(control: FormControl, validatorMessage: Array<{ key: string, message: string, default?: boolean }>) {
        if ((!control.valid && control.touched) || (control.untouched && this.formSubmitAttempt)) {
            validatorMessage = validatorMessage.filter((item) => {
                return control.hasError(item.key);
            });
            if (validatorMessage.length > 0) {
                const obj = validatorMessage.find(item => item.default);
                return obj ? obj.message : validatorMessage[0].message;
            }
        }
        return '';
    }


    handleError(error) {
        console.log('error: ', error);
        this.alertService.showMessage('Có lỗi xảy ra vui lòng thử lại sau');
    }

    showLoading(message: string) {
        this.isLoading = true;
        this.alertService.startLoadingMessage(message);
    }

    hideLoading() {
        this.isLoading = false;
        this.alertService.stopLoadingMessage();
    }
}
