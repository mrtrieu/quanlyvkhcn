import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BaocaoDetail } from 'src/app/models/baocao-detail.model';
import { VkhApiService } from 'src/app/services/vkh-api.service';
import { AlertService, AlertDialog, DialogType, MessageSeverity } from 'src/app/services/alert.service';
import { FileProjectModel, FileDetailModel } from './../../models/file-project.model';
import { FormPage } from './../controls/form-page';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Utilities } from './../../services/utilities';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { RowHeightCache } from '@swimlane/ngx-datatable/release/utils';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { Permission } from './../../models/permission.model';
import { AccountService } from './../../services/account.service';



@Component({
  selector: 'app-baocao',
  templateUrl: './baocao.component.html',
  styleUrls: ['./baocao.component.css']
})
export class BaocaoComponent implements OnInit {
  formResetToggle: boolean;

  taskEdit: BaocaoDetail = {};
  @ViewChild('editorModal')
  editorModal: ModalDirective;
  @ViewChild('editTemplate')
  editTemplate: TemplateRef<any>;
  @ViewChild('dateTemplae')
  dateTemplae: TemplateRef<any>;
  row: number;
  // multiple: boolean;

  isViewManageProject = false;
  columns: Array<any>;
  data: Array<BaocaoDetail>;
  AlertService: any;
  isAdd: boolean;
  // selectedItem: FileProjectModel;
  updateForm: FormGroup;
  progress: string;
  message: string;
  uploadId;
  dataCache: Array<BaocaoDetail>;

  constructor(
    private api: VkhApiService,
    protected alertDialog: AlertService,
    protected alertService: AlertService,
    private accountService: AccountService

  ) { }


  ngOnInit() {
    this.load();

    this.columns = [
      { name: 'Id', prop: 'id', width: 30 },
      { name: 'Mã BC', prop: 'loaibc', width: 80 },
      { name: 'Nội dung BC', prop: 'tenbc', width: 200 },
      { name: 'Ngày BC', prop: 'ngaybc', cellTemplate: this.dateTemplae, width: 80 },
      { name: 'Người BC', prop: 'nguoibc', width: 80 },
      { name: 'Action', prop: '', cellTemplate: this.editTemplate }
    ];

    this.isViewManageProject = this.accountService.userHasPermission(Permission.viewFileManagePermission);

  }

  load() {
    this.api.getBaocao().subscribe(o => {
      if (o) {
        this.dataCache = o;
        this.data = o;
        console.log('this.data : ', this.data);
      }
    });
  }
  add() {
    this.isAdd = true;
    this.formResetToggle = false;
    setTimeout(() => {
      this.formResetToggle = true;
      this.taskEdit = {};
      this.editorModal.show();
    });

  }



  save() {
    if (this.isAdd) {
      const tempUpload = this.taskEdit.fileUpload;
      const tempPublic = this.taskEdit.filePublic;
      this.taskEdit.fileUpload = null;
      this.taskEdit.filePublic = null;
      this.api.addBaocao(this.taskEdit).subscribe((baocao: BaocaoDetail) => {
        this.editorModal.hide();
        if (tempUpload && baocao) {
          tempUpload.targetId = baocao.id;
          this.api.updateFileDetail(tempUpload).subscribe(() => {
          });
        }
        if (tempPublic && baocao) {
          tempPublic.targetId = baocao.id;
          this.api.updateFileDetail(tempPublic).subscribe(() => {
          });
        }
        this.load();
      });
    } else {
      this.api.updateBaocao(this.taskEdit).subscribe(baocao => {
        this.load();
      });
    }

  }



  remove(row) {
    this.api.deleteBaocao(row.id).subscribe(baocao => {
      this.load();
    });
  }

  handleError(error) {
    console.log('error: ', error);
    this.alertDialog.showMessage('Có lỗi xảy ra vui lòng thử lại sau');
  }


  edit(row) {
    this.isAdd = false;
    this.formResetToggle = true;
    this.taskEdit = row;
    this.taskEdit.ngaybc = new Date(this.taskEdit.ngaybc);
    this.editorModal.show();


  }

  onSearchChanged(value: string) {
    this.data = this.dataCache.filter(r => Utilities.searchArray(value, false, r.tenbc, r.loaibc, r.nguoibc, r.ngaybc));
  }



  download(file: FileDetailModel) {
    if (!file) {
      this.alertService.showMessage('Lỗi', 'Chưa upload file', MessageSeverity.warn);
      return;
    }
    this.alertService.viewDocumentFile(file, () => {
      this.api.downloadFile(file.id, file.name);
    });

  }

  changeFileUpload() {
    this.taskEdit.fileUpload = null;
    this.message = '';
  }
  changeFilePublic() {
    this.taskEdit.filePublic = null;
    this.message = '';
  }

}
