import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongvandenComponent } from './congvanden.component';

describe('CongvandenComponent', () => {
  let component: CongvandenComponent;
  let fixture: ComponentFixture<CongvandenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongvandenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongvandenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
