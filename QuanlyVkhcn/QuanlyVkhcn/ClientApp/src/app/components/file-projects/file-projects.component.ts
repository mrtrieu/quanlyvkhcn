import { AppUploadComponent } from './../controls/app-upload/app-upload.component';
import { HttpEventType } from '@angular/common/http';
import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { VkhApiService } from 'src/app/services/vkh-api.service';
import { FileDetailModel, FileProjectModel, ProjectViewModel } from './../../models/file-project.model';
import { Permission } from './../../models/permission.model';
import { AccountService } from './../../services/account.service';
import { AlertService, DialogType, MessageSeverity } from './../../services/alert.service';
import { Utilities } from './../../services/utilities';
import { FormPage } from './../controls/form-page';
import { NzModalService } from 'ng-zorro-antd';
import { DocViewComponent } from '../controls/doc-view/doc-view.component';

@Component({
  selector: 'app-file-projects',
  templateUrl: './file-projects.component.html',
  styleUrls: ['./file-projects.component.css']
})
export class FileProjectsComponent extends FormPage implements OnInit, OnDestroy {


  @ViewChild('editorModal')
  editorModal: ModalDirective;
  @ViewChild('fileProjectModal')
  fileProjectModal: ModalDirective;
  @ViewChild('projectStatus')
  projectStatus: TemplateRef<any>;
  @ViewChild('dateTemplae')
  dateTemplae: TemplateRef<any>;
  @ViewChild('editFpTemplate')
  editFpTemplate: TemplateRef<any>;

  @ViewChild('fileUpload')
  fileUpload: AppUploadComponent;

  fileProjectForm: FormGroup;
  fpSelectedItem: FileProjectModel;
  isEditMode = false;

  listFileProjects: Array<FileProjectModel>;
  cacheFileProjects: Array<FileProjectModel>;
  columns: Array<any>;
  progress: string;
  message: string;

  isViewApprove = false;
  isViewSave = false;
  isViewEdit = false;
  isViewDecline = false;
  isViewManageProject = false;
  allFiles: Array<FileDetailModel>;

  projectForm: FormGroup;
  @ViewChild('projectCreateModal')
  projectCreateModal: ModalDirective;
  allProject$: Observable<ProjectViewModel[]>;

  selectedProject: ProjectViewModel;
  @ViewChild('myTable') table: any;
  isMobile: boolean;
  constructor(
    private api: VkhApiService,
    private fb: FormBuilder,
    protected alertService: AlertService,
    private accountService: AccountService,
    private modalService: NzModalService
  ) {
    super();
    this.fileProjectForm = fb.group({
      name: ['', Validators.required],
      description: [''],
      createdDate: [new Date()],
      comment: ['', Validators.required]
    });
    this.projectForm = fb.group({
      id: [null],
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  ngOnDestroy(): void {
  }

  ngOnInit() {
    this.columns = [
      { name: '#', prop: 'id', resizeable: false, canAutoResize: false, sortable: false, draggable: false, width: 60 },
      { name: 'Tên file', prop: 'name', minWidth: 150 },
      { name: 'Ngày tạo', prop: 'createdDate', cellTemplate: this.dateTemplae, minWidth: 100 },
      { name: 'Trạng thái', prop: 'status', cellTemplate: this.projectStatus, minWidth: 50, isHideMobile: true },
      { name: ' ', prop: null, cellTemplate: this.editFpTemplate, minWidth: 250, isHideMobile: true }
    ];
    if (Utilities.mediaQueryWithMaxWidth(600)) {
      this.isMobile = true;
      this.columns = this.columns.filter(o => !o.isHideMobile);
    }
    this.loadProject();
    this.isViewEdit = this.accountService.userHasPermission(Permission.viewFileEditPermission);
    this.isViewSave = this.accountService.userHasPermission(Permission.viewFileAddFilePermission);
    this.isViewManageProject = this.accountService.userHasPermission(Permission.viewFileManagePermission);
    this.isViewApprove = this.accountService.userHasPermission(Permission.viewFileApprovePermission);
    this.isViewDecline = this.accountService.userHasPermission(Permission.viewFileApprovePermission);
  }

  loadProject() {
    this.allProject$ = this.api.getProjects().pipe(tap(o => {
      if (!this.selectedProject && o) {
        this.selectedProject = o[0];
        this.loadFileProject();
      }
    }));
  }
  loadFileProject() {
    if (this.selectedProject) {
      this.api.getFileProjectsByProjectId(this.selectedProject.id)
        .subscribe(fp => {
          if (fp) {
            this.listFileProjects = [];
            this.listFileProjects = fp;
            this.cacheFileProjects = fp;
            if (this.isMobile) {
              setTimeout(() => {
                this.table.rowDetail.expandAllRows();
              }, 2000);
            }
          }
        });
    }
  }

  onDetailToggle() {

  }


  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }
  // project function

  selectProject(project) {
    if (project) {
      this.selectedProject = project;
      this.fpSelectedItem = undefined;
      this.loadFileProject();
    }
  }

  showCreateProject() {
    this.formSubmitAttempt = false;
    this.projectForm.reset();
    this.projectCreateModal.show();
  }

  createProject() {
    this.validateAllFormFields(this.projectForm);
    if (this.projectForm.valid) {
      const project = this.projectForm.value;
      if (project.id) {
        this.api.updateProject(project).subscribe(result => {
          this.projectCreateModal.hide();
          this.loadProject();
        });
      } else {
        delete project.id;
        this.api.addProject(project).subscribe(result => {
          this.projectCreateModal.hide();
          this.loadProject();
        });
      }
    }
  }

  showEditProject(project, $event) {
    $event.preventDefault();
    this.formSubmitAttempt = false;
    this.progress = '0%';
    this.projectForm.setValue({ id: project.id, name: project.name, description: project.description });
    this.projectCreateModal.show();
  }

  deleteProject(project: ProjectViewModel, $event) {
    if ($event) { $event.preventDefault(); }
    const id = project ? project.id : this.projectForm.controls['id'].value;
    this.api.deleteProject(id).subscribe(result => {
      this.alertService.showMessage('Xoá thành công');
      this.loadProject();
    }, error => {
      this.alertService.showMessage('Không thể xóa', 'Dự án có các file con.Vui lòng xóa các file con trước.', MessageSeverity.error);
    });
  }

  // end project function

  // file function
  addFP() {
    this.formSubmitAttempt = false;
    this.fpSelectedItem = {
      id: 0,
      name: '',
      description: '',
      status: 'Init',
      comment: '',
      createdDate: new Date(),
      fileUpload: null
    };
    if (this.fileUpload) { this.fileUpload.writeValue(null); }
    this.fileProjectForm.get('comment').setValidators([]);
    this.fileProjectForm.setValue({
      name: this.fpSelectedItem.name,
      description: this.fpSelectedItem.description,
      comment: this.fpSelectedItem.comment,
      createdDate: new Date()
    });
    this.fileProjectModal.show();
  }

  createFP() {
    this.formSubmitAttempt = true;
    this.validateAllFormFields(this.fileProjectForm);
    if (this.fileProjectForm.valid) {
      this.waitUpload(this.fileUpload, (file) => {
        const project: FileProjectModel = {
          name: this.fileProjectForm.get('name').value,
          description: this.fileProjectForm.get('description').value,
          createdDate: this.fileProjectForm.get('createdDate').value,
          status: file ? 'Waiting' : 'Init',
          projectId: this.selectedProject.id,
          fileUploadId: file ? file.id : ''
        };
        this.api.addFileProject(project).subscribe((newFile: FileProjectModel) => {
          if (newFile.id) {
            if (this.fpSelectedItem.fileUpload) {
              this.fpSelectedItem.fileUpload.targetId = newFile.id.toString();
            }
            this.fileProjectModal.hide();
            this.loadFileProject();
            this.api.updateFileDetail(this.fpSelectedItem.fileUpload).subscribe(() => {

            });
          }
        }, error => this.handleError(error));
      });

    }
  }

  saveFP() {
    this.formSubmitAttempt = true;
    this.validateAllFormFields(this.fileProjectForm);
    if (this.fileProjectForm.valid) {
      const newItem = Utilities.clone(this.fpSelectedItem);
      newItem.createdDate = this.fileProjectForm.get('createdDate').value;
      newItem.comment = this.fileProjectForm.get('comment').value;
      newItem.status = 'Waiting';
      newItem.assginUserId = this.accountService.currentUser.id;
      this.waitUpload(this.fileUpload, (file) => {
        if (file) {
          newItem.fileUploadId = file.id;
        }
        this.api.updateFileProject(newItem).subscribe(o => {
          this.alertService.showMessage('Cập nhật thành công');
          this.fpSelectedItem = newItem;
          this.loadFileProject();
          this.fileProjectModal.hide();
          this.hideLoading();
        });
      });
    }
  }

  verifyFP(row) {
    if (row) {
      this.fpSelectedItem = row;
    }
    this.fpSelectedItem.comment = this.fileProjectForm.get('comment').value;
    this.fpSelectedItem.status = 'Approve';
    this.fpSelectedItem.managerUserId = this.accountService.currentUser.id;
    this.waitUpload(this.fileUpload, (file) => {
      if (file) {
        this.fpSelectedItem.fileUploadId = file.id;
      }
      this.api.updateFileProject(this.fpSelectedItem).
        subscribe(o => {
          this.alertService.showMessage('Thông báo', 'Phê duyệt thành công', MessageSeverity.success);
          this.fileProjectModal.hide();
          this.hideLoading();
        });
    });
  }


  declineFP(row) {
    if (row) {
      this.fpSelectedItem = row;
      this.selectFileProject(row);
      return;
    }
    this.fpSelectedItem.comment = this.fileProjectForm.get('comment').value;
    this.fpSelectedItem.status = 'Decline';
    this.waitUpload(this.fileUpload, (file) => {
      if (file) {
        this.fpSelectedItem.fileUploadId = file.id;
      }
      this.api.updateFileProject(this.fpSelectedItem).
        subscribe(o => {
          this.alertService.showMessage('Thông báo', 'File đã bị từ chối', MessageSeverity.warn);
          this.fileProjectModal.hide();
          this.hideLoading();
        });
    });

  }

  // doi cho upload xong moi lam viet tiep theo
  waitUpload(fileUpload: AppUploadComponent, onDone: (value) => void) {
    this.showLoading('Vui lòng đợi');
    if (fileUpload) {
      fileUpload.startUpload(file => {
        onDone(file);
        this.hideLoading();
      });
      return;
    }
    onDone(null);
  }

  onSearchChanged(value) {
    this.listFileProjects = this.cacheFileProjects.filter(r => Utilities.searchArray(value, false, r.name, r.description, r.createdDate));
  }

  viewOldFile() {
    if (this.fpSelectedItem.id) {
      this.api.getFileuploadByTargetId(this.fpSelectedItem.id)
        .subscribe(result => {
          if (result) {
            this.allFiles = result as any;
          }
        });
    }
  }

  selectFileProject(row) {
    if (row) {
      this.progress = '0%';
      this.message = '';
      this.allFiles = null;
      this.fpSelectedItem = row;
      this.fileProjectForm.get('comment').setValidators([Validators.required]);
      this.fileProjectModal.show();
      this.fileProjectForm.setValue({
        name: this.fpSelectedItem.name,
        description: this.fpSelectedItem.description,
        comment: this.fpSelectedItem.comment,
        createdDate: this.fpSelectedItem.createdDate,
      });
    }
  }

  updateProject() {


  }

  upload(files) {
    if (files.length === 0) {
      return;
    }
    this.api.uploadFile(
      files[0],
      'fileproject',
      this.fpSelectedItem.id.toString(),
      this.fileProjectForm.get('comment').value
    )
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total) + '%';
        } else if (event.type === HttpEventType.Response) {
          if (event.body) {
            const result: any = event.body;
            if (result.ok) {
              this.fpSelectedItem.fileUpload = result.data;
              this.fpSelectedItem.fileUploadId = this.fpSelectedItem.fileUpload.id;
            }
            this.message = result.message;
          }
        }
      }, error => this.handleError(error));
  }

  download(file: FileDetailModel) {
    if (!file) {
      this.alertService.showMessage('Lỗi', 'Chưa upload file', MessageSeverity.warn);
      return;
    }
    this.alertService.viewDocumentFile(file, () => {
      this.api.downloadFile(file.id, file.name);
    });

  }

  deleteFP() {
    this.alertService.showDialog('Bạn có chắc muốn xóa dữ liệu ko?', DialogType.confirm, (() => {
      this.api.deleteFileProject(this.fpSelectedItem.id).subscribe(o => {
        this.fileProjectModal.hide();
        this.loadFileProject();
      });
    }));
  }

  changeFileUpload() {
    this.fpSelectedItem.fileUpload = null;
    this.fpSelectedItem.fileUploadId = null;
    this.message = '';
  }

}
