
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FilemauDetail } from 'src/app/models/filemau-detail.model';
import { VkhApiService } from 'src/app/services/vkh-api.service';
import { AlertService, AlertDialog, DialogType, MessageSeverity } from 'src/app/services/alert.service';
import { FileProjectModel, FileDetailModel } from './../../models/file-project.model';
import { FormPage } from './../controls/form-page';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Utilities } from './../../services/utilities';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { RowHeightCache } from '@swimlane/ngx-datatable/release/utils';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { Permission } from './../../models/permission.model';
import { AccountService } from './../../services/account.service';



@Component({
  selector: 'app-filemau',
  templateUrl: './filemau.component.html',
  styleUrls: ['./filemau.component.css']
})
export class FilemauComponent implements OnInit {
  [x: string]: any;
  formResetToggle: boolean;

  taskEdit: FilemauDetail = {};
  @ViewChild('editorModal')
  editorModal: ModalDirective;
  @ViewChild('editTemplate')
  editTemplate: TemplateRef<any>;
  @ViewChild('dateTemplae')
  dateTemplae: TemplateRef<any>;
  row: number;
  // multiple: boolean;

  isViewManageProject = false;

  columns: Array<any>;
  data: Array<FilemauDetail>;
  dataCache: Array<FilemauDetail>;
  AlertService: any;
  isAdd: boolean;
  // selectedItem: FileProjectModel;
  updateForm: FormGroup;
  progress: string;
  message: string;
  uploadId;


  constructor(
    private api: VkhApiService,
    protected alertDialog: AlertService,
    protected alertService: AlertService,
    private accountService: AccountService
  ) { }


  ngOnInit() {
    this.load();

    this.columns = [
      { name: 'Id', prop: 'id', width: 30 },
      { name: 'Loại File', prop: 'loaifile', width: 80 },
      { name: 'Tên File', prop: 'tenfile', width: 200 },
      { name: 'Ngày tạo', prop: 'ngaytao', cellTemplate: this.dateTemplae, width: 80 },
      { name: 'Action', prop: '', cellTemplate: this.editTemplate }
    ];

    this.isViewManageProject = this.accountService.userHasPermission(Permission.viewFileManagePermission);

  }

  load() {
    this.api.getFilemau().subscribe(o => {
      if (o) {
        this.data = o;
        this.dataCache = o;
      }
    });
  }
  add() {
    this.isAdd = true;
    this.formResetToggle = false;
    setTimeout(() => {
      this.formResetToggle = true;
      this.taskEdit = {};
      this.editorModal.show();
    });

  }

  save() {
    if (this.isAdd) {
      const tempUpload = this.taskEdit.fileUpload;
      this.taskEdit.fileUpload = null;
      this.api.addFilemau(this.taskEdit).subscribe((filemau: FilemauDetail) => {
        this.editorModal.hide();
        if (tempUpload && filemau) {
          tempUpload.targetId = filemau.id;
          this.api.updateFileDetail(tempUpload).subscribe((a) => {
          });
        }
        this.load();
      });
    } else {
      this.api.updateFilemau(this.taskEdit).subscribe(filemau => {
        this.editorModal.hide();
        this.load();
      });
    }

  }



  remove(row) {
    this.api.deleteFilemau(row.id).subscribe(filemau => {
      this.load();
    });
  }

  handleError(error) {
    console.log('error: ', error);
    this.alertDialog.showMessage('Có lỗi xảy ra vui lòng thử lại sau');
  }


  edit(row) {
    this.isAdd = false;
    this.formResetToggle = true;
    this.taskEdit = row;
    this.taskEdit.ngaytao = new Date(this.taskEdit.ngaytao);
    this.editorModal.show();


  }
  onSearchChanged(value: string) {
    this.data = this.dataCache.filter(r => Utilities.searchArray(value, false, r.loaifile, r.ngaytao, r.tenfile));
  }



  download(file: FileDetailModel) {
    if (!file) {
      this.alertService.showMessage('Lỗi', 'Chưa upload file', MessageSeverity.warn);
      return;
    }
    this.alertService.viewDocumentFile(file, () => {
      this.api.downloadFile(file.id, file.name);
    });

  }

  changeFileUpload() {
    this.taskEdit.fileUpload = null;
    this.message = '';
  }


}

