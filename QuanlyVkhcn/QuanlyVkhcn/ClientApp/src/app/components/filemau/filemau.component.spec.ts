import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilemauComponent } from './filemau.component';

describe('FilemauComponent', () => {
  let component: FilemauComponent;
  let fixture: ComponentFixture<FilemauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilemauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilemauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
