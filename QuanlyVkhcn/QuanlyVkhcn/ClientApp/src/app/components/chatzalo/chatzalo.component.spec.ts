import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatzaloComponent } from './chatzalo.component';

describe('ChatzaloComponent', () => {
  let component: ChatzaloComponent;
  let fixture: ComponentFixture<ChatzaloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatzaloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatzaloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
