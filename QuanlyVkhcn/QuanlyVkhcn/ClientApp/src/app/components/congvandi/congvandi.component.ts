import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CongvandiDetail } from 'src/app/models/congvandi-detail.model';
import { VkhApiService } from 'src/app/services/vkh-api.service';
import { AlertService, AlertDialog, DialogType, MessageSeverity } from 'src/app/services/alert.service';
import { FileProjectModel, FileDetailModel } from './../../models/file-project.model';
import { FormPage } from './../controls/form-page';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Utilities } from './../../services/utilities';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { RowHeightCache } from '@swimlane/ngx-datatable/release/utils';
import { BsDatepickerModule } from 'ngx-bootstrap';



@Component({
  selector: 'app-congvandi',
  templateUrl: './congvandi.component.html',
  styleUrls: ['./congvandi.component.css']
})
export class CongvandiComponent implements OnInit {
  formResetToggle: boolean;

  taskEdit: CongvandiDetail = {};
  @ViewChild('editorModal')
  editorModal: ModalDirective;
  @ViewChild('editTemplate')
  editTemplate: TemplateRef<any>;
  row: number;
  multiple: boolean;
  options: Array<string> = [
    'Công văn',
    'Quyết định',
    'Tờ trình',
    'Báo cáo',
    'Kế Hoạch',
    'Thông báo',
    'Phương án',
    'Đề án',
    'Hợp đồng'
  ];

  columns: Array<any>;
  data: Array<CongvandiDetail>;
  dataCache: Array<CongvandiDetail>;
  AlertService: any;
  isAdd: boolean;
  selectedItem: FileProjectModel;
  updateForm: FormGroup;
  progress: string;
  message: string;
  uploadId;


  constructor(
    private api: VkhApiService,
    protected alertService: AlertService,
    protected alertDialog: AlertService
  ) { }


  ngOnInit() {
    this.load();

    this.columns = [
      { name: 'Id', prop: 'id', width: 40 },
      { name: 'Loại CV', prop: 'loaicv', width: 70 },
      { name: 'Số CV', prop: 'socv', width: 30 },
      { name: 'Ngày ban hành', prop: 'ngaybh', width: 70 },
      { name: 'Nội dung CV', prop: 'noidungcv', width: 200 },
      { name: 'Người ký', prop: 'nguoiky', width: 80 },
      { name: 'Nơi nhận', prop: 'noinhan', width: 80 },
      { name: 'Action', prop: '', cellTemplate: this.editTemplate }
    ];
  }

  load() {
    this.api.getCongvandi().subscribe(o => {
      if (o) {
        this.data = o;
        this.dataCache = o;
      }
    });
  }
  add() {
    this.isAdd = true;
    this.formResetToggle = false;
    setTimeout(() => {
      this.formResetToggle = true;
      this.taskEdit = {};
      this.editorModal.show();
    });

  }

  dateToYMD(date) {
    if (!date.getDate) { return date; }
    const d = date.getDate();
    const m = date.getMonth() + 1; // Month from 0 to 11
    const y = date.getFullYear();
    return '' + (d <= 9 ? '0' + d : d) + '-' + (m <= 9 ? '0' + m : m) + '-' + y;
  }

  mdyToDate(date): any {
    if (!date || !date.split) { return date; }
    const st = date.split('-');
    if (st.length = 3) {
      const d = new Date();
      d.setUTCFullYear(Number.parseInt(st[2]), Number.parseInt(st[1]) - 1, Number.parseInt(st[0]));
      return d;
    }
    return new Date();
  }

  save() {
    const tempData = Utilities.clone(this.taskEdit);
    tempData.ngaybh = this.dateToYMD(tempData.ngaybh);
    if (this.isAdd) {
      const tempUpload = tempData.fileUpload;
      tempData.fileUpload = null;
      this.api.addCongvandi(tempData).subscribe((congvandi: CongvandiDetail) => {
        this.editorModal.hide();
        if (tempUpload && congvandi) {
          tempUpload.targetId = congvandi.id;
          this.api.updateFileDetail(tempUpload).subscribe((a) => {
          });
        }
        this.load();
      });
    } else {
      this.api.updateCongvandi(tempData).subscribe(congvandi => {
        this.editorModal.hide();
        this.load();
      });
    }

  }



  remove(row) {
    this.api.deleteCongvandi(row.id).subscribe(congvandi => {
      this.load();
    });
  }

  handleError(error) {
    console.log('error: ', error);
    this.alertDialog.showMessage('Có lỗi xảy ra vui lòng thử lại sau');
  }


  edit(row) {
    this.isAdd = false;
    this.formResetToggle = true;
    this.taskEdit = row;
    this.taskEdit.ngaybh = this.mdyToDate(this.taskEdit.ngaybh);
    console.log('this.taskEdit.ngaybh: ', this.taskEdit.ngaybh);
    this.editorModal.show();


  }
  onSearchChanged(value: string) {
    this.data = this.dataCache.filter(r =>
      Utilities.searchArray(value, false, r.loaicv, r.socv, r.noidungcv, r.nguoiky, r.noinhan, r.ngaybh));
  }



  download(file: FileDetailModel) {
    if (!file) {
      this.alertService.showMessage('Lỗi', 'Chưa upload file', MessageSeverity.warn);
      return;
    }
    this.alertService.viewDocumentFile(file, () => {
      this.api.downloadFile(file.id, file.name);
    });

  }

  changeFileUpload() {
    this.taskEdit.fileUpload = null;
    this.taskEdit.fileUploadId = null;
    this.message = '';
  }

}
