import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongvandiComponent } from './congvandi.component';

describe('CongvandiComponent', () => {
  let component: CongvandiComponent;
  let fixture: ComponentFixture<CongvandiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongvandiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongvandiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
